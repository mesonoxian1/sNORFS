/** @file snorfs_low_lvl.h
*
* @brief See source file.
*
* @par
* COPYRIGHT NOTICE: (c) 2018 Mihael Varga
* All rights reserved.
*/

#ifndef MY_AWESOME_DIPLOMSKI_MY_FS_LOW_LVL_H
#define MY_AWESOME_DIPLOMSKI_MY_FS_LOW_LVL_H

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------ INCLUDES -------------------------------------
#include <stdint.h>
#include <stdbool.h>
#include <sched.h>
#include <blflash.h>
//-------------------------- CONSTANTS & MACROS -------------------------------

//----------------------------- DATA TYPES ------------------------------------

//---------------------- PUBLIC FUNCTION PROTOTYPES ---------------------------
/**@brief  Reads a Page from Flash based on physical address.
 *
 * @param  p_flash[in]       - Pointer to blflash_t, unused if modifying
 *                             low-lvl access.
 * @param  page_phy_addr[in] - Physical address of a page.
 * @param  p_data[in]        - Pointer to storing location of page data.
 * @return True if succeeded, false otherwise.
 */
bool fs_low_lvl_read_page(blflash_t *p_flash,
                          size_t page_phy_addr, void *p_data);

 /**@brief Reads a Data Page from Flash based on physical address. Just calls
  *        fs_low_lvl_read_page, dprintf used for debugging, spam if dprintf
  *        used in upper function.
  *
 * @param  p_flash[in]       - Pointer to blflash_t, unused if modifying
 *                             low-lvl access.
 * @param  page_phy_addr[in] - Physical address of a page.
 * @param  p_data[in]        - Pointer to storing location of page data.
 * @return True if succeeded, false otherwise.
 */
bool fs_low_lvl_read_dp (blflash_t *p_flash,
                         size_t page_phy_addr, void *p_data);

/**@brief  Reads extended data of PP.
 *
 * @param  p_flash[in]       - Pointer to blflash_t, unused if modifying
 *                             low-lvl access.
 * @param  page_phy_addr[in] - Physical address of a page.
 * @param  p_data[in]        - Pointer to storing location of extended data.
 * @return True if succeeded, false otherwise.
 */
bool fs_low_lvl_read_extended_data (blflash_t *p_flash,
                                    size_t *p_page_phy_addr, void *p_data);

/**@brief  Reads info about DP from a PP's basic data table.
 *
 * @param  p_flash[in]       - Pointer to blflash_t, unused if modifying
 *                             low-lvl access.
 * @param  page_phy_addr[in] - Physical address of a page.
 * @param  p_data[in]        - Pointer to storing location of extended data.
 * @param  lut_pos[in]       - Position of LUT or ECT table in PP's basic data.
 * @return True if succeeded, false otherwise.
 */
bool fs_low_lvl_read_dp_info(blflash_t *p_flash, size_t page_phy_addr,
                             uint16_t lut_pos, void *p_data);

/**@brief  Writes a Page from Flash based on physical address.
 *
 * @param  p_flash[in]       - Pointer to blflash_t, unused if modifying
 *                             low-lvl access.
 * @param  page_phy_addr[in] - Physical address of a page.
 * @param  p_data[in]        - Pointer to data (PP or DP) to write.
 * @param  num_tries[in]     - Must be 0. If writing failes (MD5 check), the
 *                             function will try to re-write for 4 times, if
 *                             it fails fourth time, the flash is considered
 *                             corrupted.
 *
 * @note   Recursive function!
 * @return True if succeeded, false otherwise.
 */
bool fs_low_lvl_write_page(blflash_t *p_flash,
                           size_t page_phy_addr, const void *p_data,
                           uint8_t num_tries);

/**@brief  Erases a Page from Flash based on physical address.
 *
 * @param  p_flash[in]       - Pointer to blflash_t, unused if modifying
 *                             low-lvl access.
 * @param  page_phy_addr[in] - Physical address of a page.
 * @param  num_pages[in]     - Num of pages to erase, usually 1.
 * @return True if succeeded, false otherwise.
 */
bool fs_low_lvl_flash_erase(blflash_t *p_flash,
                            size_t page_phy_addr, size_t num_pages);


#ifdef __cplusplus
}
#endif

#endif //MY_AWESOME_DIPLOMSKI_MY_FS_LOW_LVL_H
