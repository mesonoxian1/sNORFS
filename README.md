# *sNOR FS*

 Serial NOR Flash File System with static and dynamic wear leveling.

## About
*sNOR FS* is a wear leveling FS for NOR type Flash drive of any sizes;
however it is not recommended to use this FS with large Flash sizes because
mount time and wear leveling functions will gradually slow down (32 Mbit is O.K.). Page size of used Flash device **must** be 528 B.  
This FS is based on SPI communication implemented at low level, but those
functions can be changed in order to change the method of communication. Be cautious if doing
 this - low level functions must write / erase / delete **one** page at a time!
> FS was designed to work with *blflash* library; if used with custom low level
 functions and drivers, all handling of *blflash\_t p\_flash* must be avoided and
  low level functions can then be modified for custom usage (in *low_lvl library*). To
  do this, just pass some dummy pointer at init function that is not NULL and define *blflash\_t* as *int* or something similar (typedef).

## Structure
*sNOR FS* is structured in three layers:

* *Logic layer* - *POSIX* like functions: e.g. `snorfs_fopen(args)`, `snorfs_fwrite(args)`
* *Transition layer* - translation of logical addresses into physical ones and wear
leveling algorithms.
* *Low-level layer* - low level functions for reading/writing from/to Flash.


## Usage
* 0. Initialise SPI or some other communication with Flash and make sure Flash
drivers work.
* 1. *snorfs_init*
* 2. *snorfs_mount*
* Use *snorfs_fopen*, *snorfs_fread*, *snorfs_fwrite* and other functions
acording to the function documention located in *snorfs_logic_layer.h*.

-----
> Note: FS was tested with 16 MBit NOR Flash at45db161e
   