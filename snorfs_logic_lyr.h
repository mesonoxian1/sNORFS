/** @file snorfs_logic_lyr.h
*
* @brief See source file.
*
* @par
* COPYRIGHT NOTICE: (c) 2018 Mihael Varga
* All rights reserved.
*/

#ifndef MY_AWESOME_DIPLOMSKI_MY_FS_H
#define MY_AWESOME_DIPLOMSKI_MY_FS_H

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------ INCLUDES -------------------------------------
#include <stdint.h>
#include <stdbool.h>
#include <blflash.h>

//-------------------------- CONSTANTS & MACROS -------------------------------
#define ECT_SIZE_BYTES            (3u)
#define LUT_SIZE_BYTES            (3u)
#define NUM_LUTS_IN_PP            (73u)
#define NUM_BASIC_BYTES_DATA_PAGE (512u)

#define PP_MD5_BYTES_PP (8u)
#define PP_MD5_BYTES_DP (6u)
//----------------------------- DATA TYPES ------------------------------------
/** Structure for defining a Partition Page. */
typedef struct
{
    uint8_t value[ECT_SIZE_BYTES];
} fs_ect;

typedef struct
{
    uint8_t value[LUT_SIZE_BYTES];
} fs_lut;

typedef enum
{
    FREE      = 0,
    STARTING  = 1,
    OCCUPIED  = 2,
    END_OF_DP = 3,
} page_type_t;

typedef struct
{
    fs_ect      ect;
    fs_lut      lut;
    page_type_t page_type;
} pp_page_info_t;

typedef struct
{
    char     name_pp[2];   /* Name of the Partition page. */
    fs_ect   erase_cnt;    /* Erase count of Partition page. */
    fs_lut   next_pp_addr; /* Address of next Partition page. */
    uint8_t  padding[2];
    uint8_t  md5[6];       /* 6 bytes from MD5 for Page check code. */
} pp_extended_data_t;

typedef struct
{
    pp_page_info_t     basic_data[NUM_LUTS_IN_PP]; /* 73 * 7 bytes = 511 B. */
    uint8_t            padding[1];                 /* One byte left. */
    pp_extended_data_t extended_data;              /* 16 B. */
} fs_partition_page_t;

/** Structures for defining a data page. */
typedef struct
{
    char    file_name[5];    /* Name of the file. */
    uint8_t next_dp_addr[3]; /* Address of the next page containing data if
                              * the file contains more than 512 B od data. If
                              * not, 0xFFFFFF is stored inside this variable. */
    uint16_t ending_byte;    /* Last byte of the file in the last Data page. */
    uint8_t  md5[6];         /* 6 bytes from MD5 for Page check code. */
} dp_extended_data_t;

typedef struct
{
    uint8_t            basic_data[NUM_BASIC_BYTES_DATA_PAGE];
    dp_extended_data_t extended_data;
} fs_data_page_t;

typedef struct
{
    uint32_t size_bytes;
    uint32_t size_on_flash;
    uint16_t pages_occupied;
} fs_finfo_t;

typedef enum
{
    FS_ERR_OK                = 0,  FS_UNABLE_TO_MOUNT       = 1,
    FS_FLASH_HW_ERR          = 2,  FS_PAGE_NUM_TOO_SMALL    = 3,
    FS_UNSUPPORTED_PAGE_SIZE = 4,  FS_FLASH_FORMAT_ERR      = 5,
    FS_FLASH_WRITE_ERR       = 6,  FS_FLASH_READ_ERR        = 7,
    FS_UNSUPPORTED_PAGE_ADDR = 8,  FS_FOPEN_INVALID_MODE    = 9,
    FS_FOPEN_NO_FILE         = 10, FS_FREAD_WRONG_FNAME     = 11,
    FS_FREAD_NUM_CHARS_LOW   = 12, FS_FOPEN_FILE_EXISTS     = 13,
    FS_NO_MEMORY_LEFT        = 14, FS_FILE_TOO_LARGE        = 15,
    FS_FLASH_ERASE_ERR       = 16, FS_FLASH_CORRUPTED       = 17,
    FS_INVALID_CHUNK_NUM     = 18, FS_USING_RESERVED_FNAME  = 19,
    FS_APPEND_DATA_TOO_LONG  = 20,
} fs_err_code_t;

extern blflash_t  *p_fs_flash;
extern bool       g_b_file_search;
static const char starting_partition_name[] = "fP";
static const char random_partition_name[]   = "Rp";
//---------------------- PUBLIC FUNCTION PROTOTYPES ---------------------------
/**@brief Initialises the structures used from FS. Must be called before Mount.
 *
 * @param p_fs_flash[in] - Pointer to blflash_t structure of used Flash.
 * @note  If using modified low level access without BL's functions, just
 *        pass dummy pointer which is not NULL.
 */
void snorfs_init (blflash_t *p_flash);

/**@brief  Tries to mount FS to a flash.
 *
 * @param  b_force_mount[in] - Forces a mount, formatting Flash first.
 * @return FS_ERR_OK if succeeded, see fs_err_code for other return values.
 */
fs_err_code_t snorfs_mount (bool b_force_mount);

/**@brief   Opens a file on flash to work with.
 * @details Supported modes are:
 *          "r"  - Opens a file for reading. The file must exist.
 *          "w"  - Creates an empty file for writing. If a file with the same
 *                 name already exists, error is returned.
 *          "r+" - Opens a file to update both reading and writing. The file
 *                 must exist.
 *          "w+" - Creates an empty file for both reading and writing. If the
 *                 file exists, error is returned.
 *          "a+" - Opens a file for reading and appending.
 *
 * @param  p_filename[in]  - String describing filename. Max 5 chars supported.
 * @param  p_mode[in]      - Operation mode of opened file.
 * @param  b_safe_open[in] - If true, a search for existing file will take
 *                           place, if not, Application needs to take care of
 *                           not using same filename twice. Only applies for
 *                           'w' and 'w+' modes.
 *
 * @note   If the given filename is longer then 5 chars, it will be
 *         shortened to 5 char length.
 *         Ilegal file name is (in hex) 0xffffffffff , name starting with: "fP"
 *         and name starting with "Rp"!
 *         If a large file is to be modified to something smaller, erase that
 *         file first, and reopen it, dont use 'r+'.
 * @return FS_ERR_OK if no error, associated error code otherwise.
 */
fs_err_code_t snorfs_fopen (const char *p_filename, const char *p_mode,
                            bool b_safe_open);

/**@brief  Closes the file.
 *
 * @param  p_filename[in] - String describing filename. Max 5 chars supported.
 * @return FS_ERR_OK if file is closed, FS_FREAD_WRONG_FNAME if closing wrong
 *         file.
 */
fs_err_code_t snorfs_fclose (const char *p_filename);

/**@brief  Gets the file's info. The file must be opened.
 *
 * @param  p_filename[in] - String describing filename. Max 5 chars supported.
 * @param  p_finfo[out]   - Data about how size of file, size on flash and
 *                          page count that file occupies.
 * @return FS_ERR_OK if no error, associated error code otherwise.
 */
fs_err_code_t snorfs_finfo (const char *p_filename, fs_finfo_t *p_finfo);

/**@brief  Read a file from a Flash. File must be opened!
 *
 * @param  p_saving_location[out] - Pointer to where file will be read.
 * @param  num_chars[in]          - Size of saving location, must be equal or
 *                                  bigger than file size.
 * @param  p_filename[in]         - Name of the file to read (max 5 chars).
 * @return FS_ERR_OK if no error, associated error code otherwise.
 */
fs_err_code_t snorfs_fread (void *p_saving_location, size_t num_chars,
                          const char *p_filename);

/**@brief  Writes a file to a Flash. File must be opened!
 *
 * @param  p_file[in]     - Pointer to desired file to write.
 * @param  num_chars[in]  - Size of that file.
 * @param  p_filename[in] - Name of the file to write (max 5 chars).
 * @return FS_ERR_OK if no error, associated error code otherwise.
 */
fs_err_code_t snorfs_fwrite (const char *p_file, size_t num_chars,
                             const char *p_filename);

/**@brief  Read one chunk (512 bytes) of File.
 *
 * @param  p_saving_location[out] - Pointer to where file chunk will be read.
 * @param  chunk_num[in]          - Number of chunk of data (starting from 0),
 *                                  must be contained in file, that is, file
 *                                  must have e.g. at least 3 chunks if chunk
 *                                  2 is being read.
 * @param  p_filename[in]         - Name of the file to read (max 5 chars).
 * @return FS_ERR_OK if no error, associated error code otherwise.
 */
fs_err_code_t snorfs_fread_chunk (void *p_saving_location,
                                  uint16_t chunk_num, const char *p_filename);

/**@brief  Appends a data to files end.
 *
 * @param  p_data[in]     - Pointer to data do append.
 * @param  num_chars[in]  - Number of characters to append.
 * @param  p_filename[in] - Name of the file to append (max 5 chars).
 * @return FS_ERR_OK if no error, associated error code otherwise.
 */
fs_err_code_t snorfs_fappend (const char *p_data, size_t num_chars,
                              const char *p_filename);

/**@brief  Erases file from flash.
 *
 * @param  p_filename[in] - Name of the file to read (max 5 chars).
 * @return FS_ERR_OK if no error, associated error code otherwise.
 */
fs_err_code_t snorfs_ferase (const char *p_filename);

/**@brief  Get number of free Data pages inside Flash.
 *
 * @param  p_free_dp[out] - Pointer to variable where the number will be
 *                          stored.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
fs_err_code_t snorfs_get_free_pages (uint32_t *p_free_dp);

/**@brief Attaches 8 bytes of MD5 hash to the current partition page data.
 *
 * @param curr_partition_page[in]
 */
void snorfs_attach_md5_pp (fs_partition_page_t *p_curr_partition_page);

/**@brief Attaches 8 bytes of MD5 hash to the current Data page data.
 *
 * @param curr_data_page[in]
 */
void snorfs_attach_md5_dp (fs_data_page_t *p_curr_data_page);

#ifdef __cplusplus
}
#endif

#endif //MY_AWESOME_DIPLOMSKI_MY_FS_H
