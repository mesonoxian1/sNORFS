/** @file snorfs_logic_lyr.c
*
* @brief A logical layer of sNORFS. File handling with logical addresses is
*        enabled in this module. Usage is POSIX-like. Supported file namesize
*        up to 5 characters.
*
* @par
* COPYRIGHT NOTICE: (c) 2018. Mihael Varga
* All rights reserved.
*/

//------------------------------ INCLUDES -------------------------------------
#include <snorfs_logic_lyr.h>
#include <blflash.h>
#include <blflash-chip.h>
#include <blflash-info.h>
#include <string.h>
#include <limits.h>
#include <stdio.h>
#include "snorfs_low_lvl.h"

#include "RTT.h"
#include "SEGGER_RTT.h"
#include "snorfs_logic_lyr.h"
#include "md5.h"
#include "snorfs_transition_lyr.h"

//-------------------------------- MACROS -------------------------------------
#define FILE_MODE_R  ("r")
#define FILE_MODE_W  ("w")
#define FILE_MODE_Rp ("r+")
#define FILE_MODE_Wp ("w+")
#define FILE_MODE_Ap ("a+")
//----------------------------- DATA TYPES ------------------------------------
/** Contains data about currently opened File. */
typedef struct
{
    uint32_t dp_log_addr;
    char     filename[5];
    char     file_mode[3];
} opened_file_t;
//--------------------- PRIVATE FUNCTION PROTOTYPES ---------------------------
/**@brief   Creates partition pages for newly mounted File system.
 * @details Partition pages are located on every 74th position inside Flash.
 *          Depending on number of total pages in flash, the division of PP's
 *          is created - one PP comes on 73 Data pages. One PP can address 73
 *          of Data pages, and it pointes to the next PP.
 *
 * @param   p_flash_info[in] - Pointer to Flash info from blflash_get_info.
 * @return  FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_create_partition_pages (blflash_info_t *p_flash_info);

/**@brief  Calculate the number of needed partition pages pased on total page
 *         number.
 *
 * @param  p_flash_info[in] - Pointer to Flash info from blflash_get_info.
 * @param  p_num_pp[out]    - Pointer to saving location.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_calculate_num_pp (blflash_info_t *p_flash_info,
                                          uint32_t *p_num_pp);

/**@brief  Writes the Partition page to the Flash. First erase that page,
 *         increment Erase Count and write it.
 *
 * @param  p_partition_page[in]       - Pointer to Partition page.
 * @param  physical_page_location[in] - Address of page location inside Flash.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_write_pp_flash (fs_partition_page_t *p_partition_page,
                                        uint32_t physical_page_location);

/**@brief  Opens a file to update both reading and writing. The file must exist.
 *
 * @param  p_filename[in] - String describing filename. Max 5 chars supported.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_fopen_rp (const char *p_filename);

/**@brief  Creates an empty file for both reading and writing. If file
 *         already exists, error is returned, this won't overwrite the file!
 *
 * @param  p_filename[in]  - String describing filename. Max 5 chars supported.
 * @param  b_safe_open[in] - If true, a search for existing file will take
 *                           place, if not, Application needs to take care of
 *                           not using same filename twice. Only applies for
 *                           'w' and 'w+' modes.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_fopen_wp (const char *p_filename, bool b_safe_open);

/**@brief   Search the Flash for existing file. If the file does not exist,
 *          return error.
 * @details If the file is found, data associated with it is stored in
 *          opened_file structure. That data is: Address of the DP where the
 *          file begins, address of PP pointing to that DP, position of LUT
 *          inside given PP.
 *
 * @param   p_filename[in] - String describing filename. Max 5 chars supported.
 * @return  FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_fopen_r (const char *p_filename);

/**@brief   Try to open the file for reading, if it does not exists, return
 *          error.
 * @details This function searches for first suitable location for file to
 *          be stored inside flash and allocates it. If no space is left
 *          inside Flash, error is returned.
 * @note    This function will allocate only one Data page (512 data bytes).
 *          If the file that will be stored is bigger, it will fail afterwards.
 *
 * @param   p_filename[in]  - String describing filename. Max 5 chars supported.
 * @param   b_safe_open[in] - True if search for existing file should take
 *                            place, false otherwise.
 * @return  FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_fopen_w (const char *p_filename, bool b_safe_open);

/**@brief  Opens a file for appending data.
 *
 * @param  p_filename[in] - String describing filename. Max 5 chars supported.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_fopen_ap(const char *p_filename);

/**@brief  Append data in remaining free space if File's last DP.
 *
 * @param  p_data[in]        - Pointer to data to append.
 * @param  num_chars[in]     - Number of characters of that data.
 * @param  p_finfo[in]       - Pointer to finfo structure of opened file.
 * @param  p_opened_file[in] - Pointer to opened_file_t structure.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_append_in_last_dp (const char *p_data, size_t num_chars,
                                           fs_finfo_t *p_finfo,
                                           opened_file_t *p_opened_file);

/**@brief  Append data in remaining free space if File's last DP and
 *         additional DPs needed to store the data.
 *
 * @param  p_data[in]        - Pointer to data to append.
 * @param  num_chars[in]     - Number of characters of that data.
 * @param  p_finfo[in]       - Pointer to finfo structure of opened file.
 * @param  p_opened_file[in] - Pointer to opened_file_t structure.
 * @return FS_ERR_OK if ok, see fs_err_code for all error codes.
 */
static fs_err_code_t fs_append_in_new_dps (const char *p_data, size_t num_chars,
                                           fs_finfo_t *p_finfo,
                                           opened_file_t *p_opened_file);
//----------------------- STATIC DATA & CONSTANTS -----------------------------
static const fs_lut   last_pp_marker     = {{0xff, 0xff, 0xff}};
static const fs_lut   last_dp_marker     = {{0xff, 0xff, 0xff}};
static const uint8_t  invalid_dp_addr[3] = {0xff, 0xff, 0xff};
static const uint8_t  min_page_num       = 4u;

static fs_data_page_t curr_data_page;
static pages_info_t   pages_info;   
static opened_file_t  opened_file;
static wear_lvl_t     wear_lvl;
//------------------------------ GLOBAL DATA ----------------------------------
blflash_t *p_fs_flash     = NULL;
bool      g_b_file_search = false;
//---------------------------- PUBLIC FUNCTIONS -------------------------------
void snorfs_init(blflash_t *p_flash)
{
    p_fs_flash              = p_flash;
    opened_file.dp_log_addr = 0xffffffff;

    memset(opened_file.filename,  0xff, sizeof(opened_file.filename));
    memset(opened_file.file_mode, 0xff, sizeof(opened_file.file_mode));

    wear_lvl.value_most_erased_dp  = 0; /* All pages start with 0. */
    wear_lvl.value_least_erased_dp = 0xffffffff;
    wear_lvl.log_addr_last_used_dp = 0;
    wear_lvl.log_addr_most_worn_dp = 0;

    vSemaphoreCreateBinary(flash_read_semaphore);

    if(flash_read_semaphore == NULL)
    {
        dprintf("Read Semaphore init failed\n");
    }
    else
    {
        xSemaphoreGive(flash_read_semaphore);
    }

    flash_write_semaphore = xSemaphoreCreateRecursiveMutex();

    if(flash_write_semaphore == NULL)
    {
        dprintf("Write Mutex (recursive) init failed\n");
    }
    else
    {
        xSemaphoreGive(flash_write_semaphore);
    }
}

fs_err_code_t snorfs_mount(bool b_force_mount)
{
    fs_err_code_t fs_err = FS_ERR_OK;

    if (p_fs_flash == NULL)
    {
        fs_err = FS_UNABLE_TO_MOUNT;
    }

    blflash_info_t flash_info;

    if (FS_ERR_OK == fs_err)
    {
        /* Get the flash info and do the checks. */
        blflash_error_t err = blflash_get_info(p_fs_flash, &flash_info);

        if (err)
        {
            fs_err = FS_FLASH_HW_ERR;
        }
        else
        {
            if (min_page_num > flash_info.num_pages)
            {
                return FS_PAGE_NUM_TOO_SMALL;
            }
            /* Physical Flash page size and Partition (or Data) pages must have
             * the same size. */
            if (flash_info.page_size != sizeof(fs_partition_page_t))
            {
                return FS_UNSUPPORTED_PAGE_SIZE;
            }
        }
    }

    bool fs_mounted = false;
    fs_partition_page_t starting_pp;
    /* Try to mount. */
    if ((FS_ERR_OK == fs_err) && (false == b_force_mount))
    {
        unsigned int page_cnt;

        /* Scan the entire Flash Chip for the starting Partition page. */
        for (page_cnt = 0; page_cnt < flash_info.num_pages; ++page_cnt)
        {
            /* Read whole page and then check if it's metadata matches to the
             * starting Partition page marker. */
            fs_low_lvl_read_page(p_fs_flash,
                                 page_cnt,
                                 &starting_pp);

            if (0 == memcmp(starting_pp.extended_data.name_pp,
                            starting_partition_name,
                            strlen(starting_partition_name)))
            {
                /* Store the address of the first Partition page. */
                pages_info.starting_pp_phy_addr = page_cnt;
                /* Find the PP and DP number. */
                fs_err = fs_calculate_num_pp(&flash_info, &pages_info.num_pp);

                if (FS_ERR_OK != fs_err)
                {
                    break;
                }

                pages_info.num_dp = flash_info.num_pages - pages_info.num_pp;

                /* Find out number of free pages. */
                fs_err = snorfs_get_free_pages(&pages_info.num_free_dp);

                if (FS_ERR_OK != fs_err)
                {
                    break;
                }
                /* Find most and least worn out page. */
                fs_err = tl_find_least_and_most_worn_value(&wear_lvl, &pages_info);

                if (FS_ERR_OK == fs_err)
                {
                    fs_mounted = true;
                    break;
                }
            }
        }
    }


    if ((false == fs_mounted) && (FS_ERR_OK == fs_err))
    {
        /* If the starting partition is not found, format the Flash and start
         * again. */
        dprintf(" FS not mounted, formatting Flash and trying again!\n");
        bool b_ret = fs_low_lvl_flash_erase(p_fs_flash, 0,
                                            flash_info.num_pages);

        if (false == b_ret)
        {
            return FS_FLASH_FORMAT_ERR;
        }
        else
        {
            /* Create fresh FS tables. */
            fs_err = fs_create_partition_pages(&flash_info);
            wear_lvl.value_least_erased_dp = 0;
        }
    }
    else
    {
        /* Figure out if Partition page points to another but it shouldn't;
         * that is, Flash is mounted with too little space. */
        uint32_t curr_pp_addr = pages_info.starting_pp_phy_addr;
        bool last_pp = false;

        for (uint16_t cnt_pp = 0; cnt_pp < pages_info.num_pp; ++cnt_pp)
        {
            bool b_ret = fs_low_lvl_read_page(p_fs_flash,
                                              curr_pp_addr,
                                              &starting_pp);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_READ_ERR;
                break;
            }

            if (0 == memcmp(&starting_pp.extended_data.next_pp_addr,
                            &last_pp_marker, sizeof(last_pp_marker)))
            {
                last_pp = true;
            }
            /* Continue the search in the next Partition page. */
            memcpy(&curr_pp_addr,
                   &starting_pp.extended_data.next_pp_addr,
                   sizeof(starting_pp.extended_data.next_pp_addr));
        }

        if (true != last_pp)
        {
            dprintf("Flash mounted with error: Flash size changed!\n"
                    "Erase Flash and Mount again!\n");
            fs_err = FS_UNABLE_TO_MOUNT;
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        dprintf("FS mounted!\n");
    }

    return fs_err;
}

fs_err_code_t snorfs_fopen(const char *p_filename, const char *p_mode,
                           bool b_safe_open)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    /* Sanity check. */
    if ((0 == memcmp(p_filename, starting_partition_name,
                    strlen(starting_partition_name))) ||
        (0 == memcmp(p_filename, random_partition_name,
                     strlen(random_partition_name))))
    {
        return FS_USING_RESERVED_FNAME;
    }
    /* See which mode of file is requested. */
    if (0 == memcmp(p_mode, FILE_MODE_Rp, strlen(FILE_MODE_Rp)))
    {
        fs_err = fs_fopen_rp(p_filename);
    }
    else if  (0 == memcmp(p_mode, FILE_MODE_Wp, strlen(FILE_MODE_Wp)))
    {
        fs_err = fs_fopen_wp(p_filename, b_safe_open);
    }
    else if  (0 == memcmp(p_mode, FILE_MODE_R, strlen(FILE_MODE_R)))
    {
        fs_err = fs_fopen_r(p_filename);
    }
    else if  (0 == memcmp(p_mode, FILE_MODE_W, strlen(FILE_MODE_W)))
    {
        fs_err = fs_fopen_w(p_filename, b_safe_open);
    }
    else if (0 == memcmp(p_mode, FILE_MODE_Ap, strlen(FILE_MODE_Ap)))
    {
        fs_err = fs_fopen_ap(p_filename);
    }
    else
    {
        fs_err = FS_FOPEN_INVALID_MODE;
    }

    return fs_err;
}

fs_err_code_t snorfs_fclose(const char *p_filename)
{
    /* Is this the opened file? */
    if (0 != memcmp(p_filename, opened_file.filename,
                    sizeof(opened_file.filename)))
    {
        return FS_FREAD_WRONG_FNAME;
    }
    /* Reset the Opened file data. */
    opened_file.dp_log_addr = 0xffffffff;
    memset(opened_file.filename, 0xff, sizeof(opened_file.filename));
    memset(opened_file.file_mode, 0xff, sizeof(opened_file.file_mode));

    dprintf(" === File %.5s closed ===\n", p_filename);
    return FS_ERR_OK;
}

fs_err_code_t snorfs_finfo(const char *p_filename, fs_finfo_t *p_finfo)
{
    fs_err_code_t fs_err;
    /* See if Desired filename matches the one previously opened by fopen. */
    if (0 != memcmp(p_filename, opened_file.filename,
                    sizeof(opened_file.filename)))
    {
        return FS_FREAD_WRONG_FNAME;
    }
    /* Read first data page of the file. */
    fs_err = tl_read_dp_flash(&curr_data_page, opened_file.dp_log_addr,
                              &wear_lvl, &pages_info);

    if (FS_ERR_OK != fs_err)
    {
        fs_err = FS_FLASH_READ_ERR;
    }
    /* Get the flash info. */
    blflash_info_t flash_info;
    blflash_error_t err = blflash_get_info(p_fs_flash, &flash_info);

    if (BLFLASH_ERROR_OK != err)
    {
        return FS_FLASH_HW_ERR;
    }

    p_finfo->pages_occupied = 0;
    uint32_t pages_cnt;
    /* Figure out how many data bytes does this file contain. The maximal
     * possible number of bytes is number of basic data in all Data pages. */
    for (pages_cnt = 0; pages_cnt < (flash_info.num_pages - pages_info.num_pp);
         ++pages_cnt)
    {
        p_finfo->pages_occupied++;
        /* If the next LOGICAL Data page address is invalid, this is the last
         * Data page. */
        if (0 == memcmp(curr_data_page.extended_data.next_dp_addr,
                        invalid_dp_addr,
                        sizeof(curr_data_page.extended_data.next_dp_addr)))
        {
            /* Location of last byte + 1 gives the byte size in last page.*/
            p_finfo->size_bytes =
                (p_finfo->pages_occupied - 1) *
                    sizeof(curr_data_page.basic_data) +
                        curr_data_page.extended_data.ending_byte + 1;
            break;

        }
        else
        {
            uint32_t next_dp_log_addr = 0;
            memcpy(&next_dp_log_addr, curr_data_page.extended_data.next_dp_addr,
                   sizeof(curr_data_page.extended_data.next_dp_addr));

            fs_err = tl_read_dp_flash(&curr_data_page, next_dp_log_addr,
                                      &wear_lvl, &pages_info);

            if (FS_ERR_OK != fs_err)
            {
                fs_err = FS_FLASH_READ_ERR;
                break;
            }
        }
    }

    p_finfo->size_on_flash = (p_finfo->pages_occupied) *
                             sizeof(curr_data_page.basic_data);
    return fs_err;
}

fs_err_code_t snorfs_fread(void *p_saving_location, size_t num_chars,
                           const char *p_filename)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    /* See if Desired filename matches the one previously opened by fopen. */
    if (0 != memcmp(p_filename, opened_file.filename,
                    sizeof(opened_file.filename)))
    {
        return FS_FREAD_WRONG_FNAME;
    }
    /* Check the permission mode. "w" mode will be rejected. */
    if ((0 != memcmp(FILE_MODE_Wp, opened_file.file_mode, sizeof(FILE_MODE_Wp)))
                                            &&
        (0 != memcmp(FILE_MODE_Rp, opened_file.file_mode, sizeof(FILE_MODE_Rp)))
                                            &&
        (0 != memcmp(FILE_MODE_R, opened_file.file_mode, sizeof(FILE_MODE_R)))
                                            &&
        (0 != memcmp(FILE_MODE_Ap, opened_file.file_mode, sizeof(FILE_MODE_Ap))))
    {
        return FS_FOPEN_INVALID_MODE;
    }
    /* If the num chars is smaller that the file size, return error. */
    fs_finfo_t finfo;
    fs_err = snorfs_finfo(p_filename, &finfo);

    if (finfo.size_bytes > num_chars)
    {
        return FS_FREAD_NUM_CHARS_LOW;
    }

    uint32_t pages_cnt;
    uint32_t curr_dp_addr = opened_file.dp_log_addr;
    /* Copy the whole file in location pointed by saving_location. */
    for (pages_cnt = 0; pages_cnt < finfo.pages_occupied; ++pages_cnt)
    {
        fs_err = tl_read_dp_flash(&curr_data_page, curr_dp_addr, &wear_lvl,
                                  &pages_info);

        if (FS_ERR_OK != fs_err)
        {
            fs_err = FS_FLASH_READ_ERR;
            break;
        }

        if (0 == memcmp(curr_data_page.extended_data.next_dp_addr,
                        invalid_dp_addr, sizeof(invalid_dp_addr)))
        {
            /* If this is the last data page. */
            memcpy(p_saving_location +
                     (pages_cnt * sizeof(curr_data_page.basic_data)),
                   curr_data_page.basic_data,
                   curr_data_page.extended_data.ending_byte + 1u);
        }
        else
        {
            /* Just copy the whole Basic info part of Data page. */
            memcpy(p_saving_location +
                      (pages_cnt * sizeof(curr_data_page.basic_data)),
                   curr_data_page.basic_data,
                   sizeof(curr_data_page.basic_data));

            memcpy(&curr_dp_addr, curr_data_page.extended_data.next_dp_addr,
                   LUT_SIZE_BYTES);
        }
    }

    return fs_err;
}

fs_err_code_t snorfs_fwrite(const char *p_file, size_t num_chars,
                            const char *p_filename)
{
    fs_err_code_t fs_err = FS_ERR_OK;

    /* See if Desired filename matches the one previously opened by fs_fopen. */
    if (0 != memcmp(p_filename, opened_file.filename,
                    sizeof(opened_file.filename)))
    {
        return FS_FREAD_WRONG_FNAME;
    }
    /* Check the permission mode. FILE_MODE_W will pass for both "w" and
     * "w+", and "r+" makes sure "r" mode will be rejected. */
    if ((0 != memcmp(FILE_MODE_W, opened_file.file_mode, sizeof(FILE_MODE_W)))
                                        &&
       (0 != memcmp(FILE_MODE_Wp, opened_file.file_mode, sizeof(FILE_MODE_Wp)))
                                        &&
       (0 != memcmp(FILE_MODE_Rp, opened_file.file_mode, sizeof(FILE_MODE_Rp))))
    {
        return FS_FOPEN_INVALID_MODE;
    }
    /* Check if free space on Flash is enough to store the file. */
    uint32_t pages_occupied = 0;
    /* If the file size if a multiple of 512 bytes. */
    if (0 == (num_chars % NUM_BASIC_BYTES_DATA_PAGE))
    {
        /* Casted because num_chars is size_t, the values will fit inside
         * pages_occupied. */
        pages_occupied = (uint16_t) (num_chars / NUM_BASIC_BYTES_DATA_PAGE);
        /* +1 because one page is allocated when file is opened. */
        if ((pages_info.num_free_dp + 1) < pages_occupied)
        {
            return FS_FILE_TOO_LARGE;
        }
    }
    else
    {
        /* Otherwise one more Data page is needed. */
        /* Casted because num_chars is size_t, the values will fit inside
         * pages_occupied. */
        pages_occupied = (uint16_t) (num_chars / NUM_BASIC_BYTES_DATA_PAGE + 1);

        if ((pages_info.num_free_dp + 1) < pages_occupied)
        {
            return FS_FILE_TOO_LARGE;
        }
    }

    uint32_t    curr_dp_log_addr = opened_file.dp_log_addr;
    page_type_t page_type;

    for (uint16_t cnt_pages = 0; cnt_pages < pages_occupied; ++cnt_pages)
    {
        memset(&curr_data_page, 0xff, sizeof(curr_data_page));
        /* When preparing the data for last Data page. */
        if ((cnt_pages + 1) == pages_occupied)
        {
            page_type = OCCUPIED;
            /* Copy the remaining data. */

            /* When data is prepared, set the ending byte for FS. */
            if (0 == (num_chars % NUM_BASIC_BYTES_DATA_PAGE))
            {
                memcpy(curr_data_page.basic_data,
                       &p_file[cnt_pages * NUM_BASIC_BYTES_DATA_PAGE],
                       NUM_BASIC_BYTES_DATA_PAGE);
                curr_data_page.extended_data.ending_byte = 511u;
            }
            else
            {
                memcpy(curr_data_page.basic_data,
                       &p_file[cnt_pages * NUM_BASIC_BYTES_DATA_PAGE],
                       num_chars % NUM_BASIC_BYTES_DATA_PAGE);
                curr_data_page.extended_data.ending_byte = (uint16_t)
                        ((num_chars % NUM_BASIC_BYTES_DATA_PAGE) - 1u);
            }

            if (0 == cnt_pages)
            {
                /* This is the only Data page needed for file storage,
                 * therefore it is first and last at the same time. */
                memset(curr_data_page.extended_data.file_name, 0,
                       sizeof(curr_data_page.extended_data.file_name));
                memcpy(curr_data_page.extended_data.file_name, p_filename,
                       strlen(p_filename));
                /* If this isn't the case, the name will remain 0xffffff. */
                page_type = STARTING;
            }
            /* Next DP Address is already set to 0xff's. Only attach MD5. */
            snorfs_attach_md5_dp(&curr_data_page);
            /* Write the prepared Data page to Flash. */
            dprintf("Writing DP in LOG addr: %d\n", curr_dp_log_addr);
            fs_err = tl_write_dp_flash(&curr_data_page, curr_dp_log_addr,
                                       page_type, &wear_lvl, &pages_info);

            if ((FS_ERR_OK == fs_err) && (0 != cnt_pages))
            {
                pages_info.num_free_dp--;
            }
        }
        else if (0 == cnt_pages)
        {
            /* If preparing the data for first Data page, copy the starting
             * data bytes. This Data page must contain all 512 data bytes. */
            memcpy(curr_data_page.basic_data, p_file,
                   NUM_BASIC_BYTES_DATA_PAGE);
            /* Setup the file name. */
            memset(curr_data_page.extended_data.file_name, 0,
                   sizeof(curr_data_page.extended_data.file_name));
            memcpy(curr_data_page.extended_data.file_name, p_filename,
                   strlen(p_filename));
            /* Set the next Data page address. */
            uint32_t next_free_dp = 0;
            fs_err = tl_get_free_page_log_addr(&next_free_dp, &wear_lvl,
                                               &pages_info,
                                               true, curr_dp_log_addr);

            if (FS_ERR_OK != fs_err)
            {
                return fs_err;
            }

            memcpy(curr_data_page.extended_data.next_dp_addr, &next_free_dp,
                   sizeof(curr_data_page.extended_data.next_dp_addr));
            /* Ending byte is irrelevant since this is not the last Data page.*/
            snorfs_attach_md5_dp(&curr_data_page);
            /* Write the prepared Data page to Flash. */
            dprintf("Writing DP in LOG addr: %d\n", curr_dp_log_addr);
            fs_err = tl_write_dp_flash(&curr_data_page, curr_dp_log_addr,
                                       STARTING, &wear_lvl, &pages_info);
            curr_dp_log_addr = next_free_dp;
        }
        else {
            /* Otherwise prepare the whole Data page. */
            memcpy(curr_data_page.basic_data,
                   &p_file[cnt_pages * NUM_BASIC_BYTES_DATA_PAGE],
                   NUM_BASIC_BYTES_DATA_PAGE);
            /* Set the next Data page address. */
            uint32_t next_free_dp;
            fs_err = tl_get_free_page_log_addr(&next_free_dp, &wear_lvl,
                                               &pages_info,
                                               true, curr_dp_log_addr);

            if (FS_ERR_OK != fs_err) {
                return fs_err;
            }

            memcpy(curr_data_page.extended_data.next_dp_addr, &next_free_dp,
                   sizeof(curr_data_page.extended_data.next_dp_addr));
            /* File ending byte is irrelevant since this is not the last Data
             * page. */
            snorfs_attach_md5_dp(&curr_data_page);
            dprintf("Writing DP in LOG addr: %d\n", curr_dp_log_addr);
            /* Write the prepared Data page to Flash. */
            fs_err = tl_write_dp_flash(&curr_data_page, curr_dp_log_addr,
                                       OCCUPIED, &wear_lvl, &pages_info);

            if (FS_ERR_OK == fs_err)
            {
                pages_info.num_free_dp--;
                curr_dp_log_addr = next_free_dp;
            }
        }

        if (FS_ERR_OK != fs_err)
        {
            break;
        }
    }

    return fs_err;
}

fs_err_code_t snorfs_fread_chunk(void *p_saving_location,
                                 uint16_t chunk_num, const char *p_filename)
{
    fs_err_code_t fs_err;
    /* See if filename matches the one previously opened by fs_fopen. */
    if (0 != memcmp(p_filename, opened_file.filename,
                    sizeof(opened_file.filename)))
    {
        return FS_FREAD_WRONG_FNAME;
    }

    fs_finfo_t finfo;
    fs_err = snorfs_finfo(p_filename, &finfo);

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }

    if ((chunk_num > finfo.pages_occupied) && (1u > chunk_num))
    {
        return FS_INVALID_CHUNK_NUM;
    }

    uint32_t       curr_dp_log_addr = opened_file.dp_log_addr;
    fs_data_page_t data_page;
    /* Find the DP containing that chunk. */
    for (uint32_t cnt = 0; cnt < chunk_num; ++cnt)
    {
        fs_err = tl_read_dp_flash(&data_page, curr_dp_log_addr,
                                  &wear_lvl, &pages_info);

        if (FS_ERR_OK != fs_err)
        {
            break;
        }

        memcpy(&curr_dp_log_addr, data_page.extended_data.next_dp_addr,
               LUT_SIZE_BYTES);
    }

    if (FS_ERR_OK == fs_err)
    {
        /* The desired chunk has been found - copy it. */
        memcpy(p_saving_location, data_page.basic_data,
               sizeof(data_page.basic_data));
    }

    return fs_err;
}

fs_err_code_t snorfs_fappend(const char *p_data, size_t num_chars,
                             const char *p_filename)
{
    fs_err_code_t fs_err;
    /* See if filename matches the one previously opened by fs_fopen. */
    if (0 != memcmp(p_filename, opened_file.filename,
                    sizeof(opened_file.filename)))
    {
        return FS_FREAD_WRONG_FNAME;
    }
    /* Sanity check on File mode. */
    if (0 != memcmp(FILE_MODE_Ap, opened_file.file_mode, sizeof(FILE_MODE_Ap)))
    {
        return FS_FOPEN_INVALID_MODE;
    }

    fs_finfo_t finfo;
    fs_err = snorfs_finfo(p_filename, &finfo);

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }

    if ((num_chars / NUM_BASIC_BYTES_DATA_PAGE) < pages_info.num_free_dp)
    {
        /* Possible to append data in existing DP even if no DPs are free. */
        return FS_APPEND_DATA_TOO_LONG;
    }
    /** Two possible cases: appended data will fit inside space of last DP,
     *  or new DP will be required. */
    uint32_t num_bytes_in_last_dp;
    /* Find out the number of the bytes in the last DP. */
    num_bytes_in_last_dp = finfo.size_on_flash - finfo.size_bytes;

    if (num_chars <= num_bytes_in_last_dp)
    {
        fs_err = fs_append_in_last_dp(p_data, num_chars, &finfo, &opened_file);
    }
    else
    {
        fs_err = fs_append_in_new_dps(p_data, num_chars, &finfo, &opened_file);

        if (FS_ERR_OK == fs_err)
        {
            pages_info.num_free_dp -= (num_chars / NUM_BASIC_BYTES_DATA_PAGE)
                                     + 1u; /* 1 DP minimal. */
        }
    }

    return fs_err;
}

fs_err_code_t snorfs_ferase(const char *p_filename)
{
    fs_err_code_t fs_err;
    /* See if filename matches the one previously opened by fs_fopen. */
    if (0 != memcmp(p_filename, opened_file.filename,
                    sizeof(opened_file.filename)))
    {
        return FS_FREAD_WRONG_FNAME;
    }

    fs_finfo_t finfo;
    fs_err = snorfs_finfo(p_filename, &finfo);

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }

    uint32_t       temp_dp_log_addr = opened_file.dp_log_addr;
    fs_data_page_t data_page;
    /* Erase all the pages containing data of given file. */
    for (uint32_t cnt = 0; cnt < finfo.pages_occupied; ++cnt)
    {
        fs_err = tl_read_dp_flash(&data_page, temp_dp_log_addr, &wear_lvl,
                                  &pages_info);

        if (FS_ERR_OK != fs_err)
        {
            break;
        }

        fs_err = tl_erase_dp_flash(temp_dp_log_addr, &wear_lvl, &pages_info);

        if (FS_ERR_OK != fs_err)
        {
            break;
        }

        memcpy(&temp_dp_log_addr, data_page.extended_data.next_dp_addr,
               LUT_SIZE_BYTES);
    }

    return fs_err;
}

fs_err_code_t snorfs_get_free_pages(uint32_t *p_free_dp)
{
    fs_err_code_t fs_err        = FS_ERR_OK;
    uint32_t    free_pages    = 0;
    bool        b_reached_end = false;
    static      fs_partition_page_t partition_page;


    memcpy(&pages_info.addr_pp[0], &pages_info.starting_pp_phy_addr,
           LUT_SIZE_BYTES);

    /* Search for all free pages to get byte count. */
    for (uint32_t cnt_pp = 0; cnt_pp < pages_info.num_pp; ++cnt_pp)
    {
        /* Search every 74th logical position (every PP). */
        fs_err = tl_read_pp_flash(&partition_page,
                                  cnt_pp * (NUM_LUTS_IN_PP + 1),
                                  &wear_lvl, &pages_info);

        if (FS_ERR_OK != fs_err)
        {
            return FS_FLASH_READ_ERR;
        }

        if (cnt_pp < (pages_info.num_pp - 1u))
        {
            memcpy(&pages_info.addr_pp[cnt_pp + 1u],
                   &partition_page.extended_data.next_pp_addr, LUT_SIZE_BYTES);
        }

        for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
        {
            if (0 == memcmp(&last_dp_marker,
                            &partition_page.basic_data[lut_cnt].lut,
                            LUT_SIZE_BYTES))
            {
                b_reached_end = true;
                break;
            }

            if (FREE == partition_page.basic_data[lut_cnt].page_type)
            {
                free_pages++;
            }
        }

        if (true == b_reached_end)
        {
            break;
        }
    }

    if (0 == free_pages)
    {
        fs_err = FS_NO_MEMORY_LEFT;
    }

    *p_free_dp = free_pages;
    return fs_err;
}

void snorfs_attach_md5_pp(fs_partition_page_t *p_curr_partition_page)
{
    uint8_t temp_md5[PP_MD5_BYTES_PP];
    /* Interpreting whole array (without MD5 data) as a string, so cast is
     * legit. */
    calc_md5((uint8_t *) p_curr_partition_page,
             sizeof(fs_partition_page_t) - PP_MD5_BYTES_PP, temp_md5);

    memcpy(p_curr_partition_page->extended_data.md5, temp_md5, PP_MD5_BYTES_DP);
}

void snorfs_attach_md5_dp(fs_data_page_t *p_curr_data_page)
{
    /* Calculate all 8 bytes, that is why PP is used. */
    uint8_t temp_md5[PP_MD5_BYTES_PP];
    /* Interpreting whole array (without MD5 data) as a string, so cast is
     * legit. */
    calc_md5((uint8_t *) p_curr_data_page,
             sizeof(fs_partition_page_t) - PP_MD5_BYTES_PP, temp_md5);

    memcpy(p_curr_data_page->extended_data.md5, temp_md5, PP_MD5_BYTES_DP);
}
//--------------------------- PRIVATE FUNCTIONS -------------------------------
static fs_err_code_t fs_create_partition_pages (blflash_info_t *p_flash_info)
{
    fs_err_code_t fs_err;
    /* Figure out how many partition pages must there be in Flash. One
     * partition page can hold LUT for maximum of 73 data pages. */
    fs_err = fs_calculate_num_pp(p_flash_info, &pages_info.num_pp);

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }

    pages_info.num_dp      = p_flash_info->num_pages - pages_info.num_pp;
    pages_info.num_free_dp = pages_info.num_dp; /* All of the pages are free. */
    /* This value represents Data page address value that will be stored in a
     * LUT of a Partition page addressing that Data page. It is also used to
     * address physical location where the certain partition page will be
     * burned during the mount process. */
    unsigned int        lut_num = 1;
    fs_partition_page_t curr_pp;

    for (uint16_t cnt_pp = 0; cnt_pp < pages_info.num_pp; ++cnt_pp)
    {
        /* Setup the data of starting Physical PPs addresses. */
        uint32_t pp_addr = cnt_pp * (NUM_LUTS_IN_PP + 1u);
        memcpy(&pages_info.addr_pp[cnt_pp], &pp_addr, LUT_SIZE_BYTES);
        /** Setup the basic data (LUT, ECT etc). */
        for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
        {
            curr_pp.basic_data[lut_cnt].page_type = FREE;
            memset(curr_pp.basic_data[lut_cnt].ect.value, 0,
                   sizeof(curr_pp.basic_data[lut_cnt].ect.value));

            memcpy(&curr_pp.basic_data[lut_cnt].lut, &lut_num,
                   sizeof(curr_pp.basic_data[lut_cnt].lut));

            lut_num++;
            /* Check if the lut_num (number of addressed Data pages) has
             * reached the maximum of pages that can be addressed (stored). */
            if ((lut_num > (p_flash_info->num_pages)) &&
                (0 != (lut_num % (NUM_LUTS_IN_PP + 1u))))
            {
                /** No more room for Data pages inside Flash. Since the
                  * invalid address in a LUT is marked with 0xffffff, and the
                  * State of Partition page is initially like that after the
                  * burn, this is handled automatically. */
                /* First set the lut_num to a value it would had if itc
                 * reached end and then exit loop. */
                uint16_t val = (uint16_t) (lut_num % (NUM_LUTS_IN_PP + 1u));
                val          = (uint16_t) (NUM_LUTS_IN_PP + 1u) - val;
                lut_num     += val;
                /* Set the remaining data to 0xff. */
                memset(&curr_pp.basic_data[lut_cnt], 0xff,
                       (NUM_LUTS_IN_PP - lut_cnt) *
                       sizeof(curr_pp.basic_data[0]));
                curr_pp.basic_data[lut_cnt].page_type = END_OF_DP;

                memset(&curr_pp.basic_data[++lut_cnt], 0xff,
                       (NUM_LUTS_IN_PP - lut_cnt) *
                                 sizeof(curr_pp.basic_data[0]));
                curr_pp.basic_data[lut_cnt].page_type = END_OF_DP;
                /* Rewrite the basic data with data describing last Data page
                 * in LUT. */
                break;
            }
        }
        /** Setup the extended data. */
        if (0 == cnt_pp)
        {
            /* If writing in first Partition page. */
            memcpy(curr_pp.extended_data.name_pp, starting_partition_name,
                   sizeof(curr_pp.extended_data.name_pp));
        }
        else
        {
            /* If writing in some other Partition page. */
            memcpy(curr_pp.extended_data.name_pp, random_partition_name,
                   sizeof(curr_pp.extended_data.name_pp));
        }

        memset(&curr_pp.extended_data.erase_cnt, 0, ECT_SIZE_BYTES);
        /* Next Partition page will be physically stored after the first
         * 74 pages. */
        memcpy(curr_pp.extended_data.next_pp_addr.value, &lut_num,
               sizeof(curr_pp.extended_data.next_pp_addr));

        /* If this is the last partition page mark it as that. */
        if (lut_num >= p_flash_info->num_pages) //ili if cnt_pp == (num_pp -1)
        {
            /* Oxffffff marks the last Partition page. */
            memcpy(&curr_pp.extended_data.next_pp_addr, &last_pp_marker,
                   LUT_SIZE_BYTES);
        }
        /* Calculate MD5 for current page and attach it. */
        snorfs_attach_md5_pp(&curr_pp);
        /* Burn the Current page to the Flash! Partition pages are burned on
         * every 74th page inside flash during initial mount. */
        fs_err = fs_write_pp_flash(&curr_pp, lut_num - (NUM_LUTS_IN_PP + 1));

        if (FS_ERR_OK != fs_err)
        {
            break;
        }

        if (0 == cnt_pp)
        {
            /* Store the temp data of the first Partition Page. */
            pages_info.starting_pp_phy_addr = 0;
        }

        lut_num++;
    }

    return fs_err;
}

static fs_err_code_t fs_calculate_num_pp (blflash_info_t *p_flash_info,
                                          uint32_t *p_num_pp)
{
    fs_err_code_t err = FS_ERR_OK;
    /* Sanity check. */
    if (min_page_num > p_flash_info->num_pages)
    {
        /* Usually this should never happen if Flash is being mounted in a
         * correct way. */
        return FS_PAGE_NUM_TOO_SMALL;
    }

    uint32_t num_pp = 0;

    if ((NUM_LUTS_IN_PP + 1) >= p_flash_info->num_pages)
    {
        /* FS needs at least one Partition page. */
        num_pp = 1;
    }
    else if (0 == (p_flash_info->num_pages % (NUM_LUTS_IN_PP + 1)))
    {
        /* If total page number is a multiple of 74 - 73 Data pages can be
         * addressed from one Partition page. */
        num_pp = p_flash_info->num_pages / (NUM_LUTS_IN_PP + 1);
    }
    else
    {
        num_pp = p_flash_info->num_pages / (NUM_LUTS_IN_PP + 1);
        /* An extra Partition page must be added, even if it addresses only
         * one data page. */
        num_pp++;
    }
    /* File System supported number of Partition pages can't be bigger than
     * (0xffffff - 1)!  0xFFFFFF is reserved. */
    if ((0xffffff - 1) <= num_pp)
    {
        err = FS_UNSUPPORTED_PAGE_SIZE;
    }
    else
    {
        /* Otherwise the value fits inside 24 bit number. */
        *p_num_pp = num_pp;
    }

    return err;
}

static fs_err_code_t fs_write_pp_flash (fs_partition_page_t *p_partition_page,
                                        uint32_t physical_page_location)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    /* Sanity check. */
    blflash_info_t flash_info;
    blflash_error_t flash_err = blflash_get_info(p_fs_flash, &flash_info);

    if (BLFLASH_ERROR_OK != flash_err)
    {
        dprintf("Flash error during write\n");
        fs_err = FS_FLASH_HW_ERR;
    }

    if (physical_page_location > flash_info.num_pages)
    {
        dprintf("Flash error: unsupported page address\n");
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (FS_ERR_OK == fs_err)
    {

        /* Erase the page. */
        fs_low_lvl_flash_erase(p_fs_flash, physical_page_location, 1);
        /* Increment this value on every write operation. */
        uint32_t erase_count;
        memcpy(&erase_count, p_partition_page->extended_data.erase_cnt.value,
               ECT_SIZE_BYTES);
        erase_count++;
        memcpy(p_partition_page->extended_data.erase_cnt.value, &erase_count,
               ECT_SIZE_BYTES);

        snorfs_attach_md5_pp(p_partition_page);
        bool b_ret;
        /* Burn the data to the Flash! */
        b_ret = fs_low_lvl_write_page(p_fs_flash, physical_page_location,
                                      p_partition_page, 0);

        if (false == b_ret)
        {
            dprintf("Flash error during write\n");
            fs_err = FS_FLASH_WRITE_ERR;
        }

        if (true == b_ret)
        {
            pp_extended_data_t extended_data;
            memcpy(&extended_data,
                   &p_partition_page->extended_data, sizeof(extended_data));
            /* Re-read page and make sure it was burned correctly! */
            fs_partition_page_t temp_pp;
            fs_low_lvl_read_page(p_fs_flash, physical_page_location, &temp_pp);

            if (0 != memcmp(&temp_pp, p_partition_page, sizeof(temp_pp)))
            {
                dprintf("Error while writing PP!\n");
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }
    }

    return fs_err;
}

static fs_err_code_t fs_fopen_rp (const char *p_filename)
{
    fs_err_code_t fs_err;
    /* Search the Flash for existing file. If the file does not exist,
     * return error. */
    fs_err = fs_fopen_r(p_filename);

    if (FS_ERR_OK == fs_err)
    {
        memcpy(opened_file.file_mode, FILE_MODE_Rp, sizeof(FILE_MODE_Rp));
    }

    return fs_err;
}

static fs_err_code_t fs_fopen_wp (const char *p_filename, bool b_safe_open)
{
    fs_err_code_t fs_err;
    /* Creates an empty file for writing and reading, if the file exists,
     * error is returned. */
    fs_err = fs_fopen_w(p_filename, b_safe_open);

    if (FS_FOPEN_FILE_EXISTS == fs_err)
    {
        /* The file was opened for reading, but since 'w+' mode specifies that
         * that mode creates an empty file, close existing file and return
         * error. */
        snorfs_fclose(p_filename);
    }

    if ((FS_ERR_OK == fs_err))
    {
        memcpy(opened_file.file_mode, FILE_MODE_Wp, sizeof(FILE_MODE_Wp));
    }

    return fs_err;
}

static fs_err_code_t fs_fopen_r (const char *p_filename)
{
    /* The premise is that file doesn't exists. */
    fs_err_code_t fs_err = FS_FOPEN_NO_FILE;
    /* Search the Flash for existing file. If the file does not exist,
     * return error. */
    blflash_info_t flash_info;
    blflash_error_t flash_err = blflash_get_info(p_fs_flash, &flash_info);

    if (BLFLASH_ERROR_OK != flash_err)
    {
        dprintf("Flash error during write\n");
        return FS_FLASH_HW_ERR;
    }

    uint32_t num_dp = flash_info.num_pages - pages_info.num_pp;
    g_b_file_search = true;
    static fs_data_page_t temp_dp;
    uint32_t dp_cnt;
    fs_partition_page_t temp_pp;

    for (dp_cnt = 0; dp_cnt < num_dp; ++dp_cnt)
    {
        if (0 == (dp_cnt % (NUM_LUTS_IN_PP + 1u)))
        {
            /* Read the associated PP. */
            memset(&temp_pp, 0xff, sizeof(temp_pp));
            fs_err = tl_read_pp_flash(&temp_pp, dp_cnt, &wear_lvl, &pages_info);

            if (fs_err != FS_ERR_OK)
            {
                return fs_err;
            }
            else
            {
                fs_err = FS_FOPEN_NO_FILE; /* Reset the premise to 'No file'. */
            }
            /* Avoid every PP logical address (multiple of 74). */
        }
        else
        {
            /* Only read DP if it contains starting data. */
            if (STARTING == temp_pp.basic_data[
                               (dp_cnt % (NUM_LUTS_IN_PP + 1u)) - 1].page_type)
            {
                /* Starting from '1'. */
                fs_err = tl_read_dp_flash(&temp_dp, dp_cnt, &wear_lvl,
                                          &pages_info);

                if (fs_err != FS_ERR_OK)
                {
                    return fs_err;
                }
                else
                {
                    /* Reset the premise to 'No file'. */
                    fs_err = FS_FOPEN_NO_FILE;
                }

                if (0 == memcmp(temp_dp.extended_data.file_name, p_filename,
                                strlen(p_filename)))
                {
                    /* The file has been found! */
                    opened_file.dp_log_addr = dp_cnt;
                    memcpy(opened_file.file_mode, FILE_MODE_R,
                           sizeof(FILE_MODE_R));
                    memcpy(opened_file.filename, p_filename,
                           sizeof(opened_file.filename));

                    dprintf("File '%s' opened on Logical addr: %d\n",
                            p_filename, dp_cnt);
                    g_b_file_search = false;
                    return FS_ERR_OK;
                }
            }
        }
    }

    g_b_file_search = false;
    return fs_err;
}

static fs_err_code_t fs_fopen_w (const char *p_filename, bool b_safe_open)
{
    fs_err_code_t fs_err = FS_FOPEN_NO_FILE;
    /* Try to open the file for reading, if it exists, return error. */
    if (true == b_safe_open)
    {
        fs_err = fs_fopen_r(p_filename);

        if ((FS_ERR_OK != fs_err) && (FS_FOPEN_NO_FILE != fs_err))
        {
            return fs_err;
        }
    }

    if (0 == pages_info.num_free_dp)
    {
        return FS_NO_MEMORY_LEFT;
    }

    if (FS_FOPEN_NO_FILE == fs_err)
    {
        /* Search for first suitable location inside Flash and allocate it. */
        uint32_t free_dp_log_addr = 0;
        fs_err = tl_get_free_page_log_addr(&free_dp_log_addr,
                                           &wear_lvl, &pages_info, false, 0);

        if (FS_ERR_OK != fs_err)
        {
            return fs_err;
        }

        opened_file.dp_log_addr = free_dp_log_addr;
        memcpy(opened_file.filename, p_filename, sizeof(opened_file.filename));
        memcpy(opened_file.file_mode, FILE_MODE_W, sizeof(FILE_MODE_W));

        static fs_data_page_t data_page;
        memset(&data_page, 0xff, sizeof(data_page));
        /* If the data page is free, it's previous content doesn't matter; just
         * prepare the new data. */
        memset(curr_data_page.extended_data.file_name, 0,
               sizeof(curr_data_page.extended_data.file_name));
        memcpy(curr_data_page.extended_data.file_name, p_filename,
               strlen(p_filename));
        data_page.extended_data.ending_byte = 0; /* No data in the page. */
        snorfs_attach_md5_dp(&data_page);
        /* Write that page. */
        fs_err = tl_write_dp_flash(&data_page, free_dp_log_addr,
                                   STARTING, &wear_lvl, &pages_info);
        /* Allocate the DP for file. */
        pages_info.num_free_dp--;

    }
    else
    {
        fs_err = FS_FOPEN_FILE_EXISTS;
    }
    return fs_err;
}

static fs_err_code_t fs_fopen_ap(const char *p_filename)
{
    fs_err_code_t fs_err;
    /* Try to open the file, if it does not exist, return error! */
    fs_err = fs_fopen_r(p_filename);

    if (FS_ERR_OK == fs_err)
    {
        memcpy(opened_file.file_mode, FILE_MODE_Ap, sizeof(FILE_MODE_Ap));
    }

    return fs_err;
}

static fs_err_code_t fs_append_in_last_dp (const char *p_data, size_t num_chars,
                                           fs_finfo_t *p_finfo,
                                           opened_file_t *p_opened_file)
{
    fs_err_code_t    fs_err;
    uint32_t       curr_dp_log_addr = p_opened_file->dp_log_addr;
    fs_data_page_t data_page;
    /* Read the File's last DP. */
    for (uint32_t cnt = 0; cnt < p_finfo->pages_occupied; ++cnt)
    {
        fs_err = tl_read_dp_flash(&data_page, curr_dp_log_addr,
                                  &wear_lvl, &pages_info);

        if (FS_ERR_OK != fs_err)
        {
            break;
        }

        if (0 == memcmp(data_page.extended_data.next_dp_addr, &last_dp_marker,
                        LUT_SIZE_BYTES))
        {
            /* This has been the last DP. */
            break;
        }

        memcpy(&curr_dp_log_addr, data_page.extended_data.next_dp_addr,
               LUT_SIZE_BYTES);
    }
    /* Copy the data in the last DP and write that page to the Flash. */
    memcpy(&data_page.basic_data[data_page.extended_data.ending_byte + 1u],
           p_data, num_chars);
    snorfs_attach_md5_dp(&data_page);

    page_type_t page_type = OCCUPIED;

    if (opened_file.dp_log_addr == curr_dp_log_addr)
    {
        page_type = STARTING; /* This is starting page. */
    }

    return tl_write_dp_flash(&data_page, curr_dp_log_addr, page_type,
                             &wear_lvl, &pages_info);
}

static fs_err_code_t fs_append_in_new_dps (const char *p_data, size_t num_chars,
                                           fs_finfo_t *p_finfo,
                                           opened_file_t *p_opened_file)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    uint32_t    num_accupied_dp;
    uint32_t    num_chars_left_first_dp =
                             p_finfo->size_on_flash - p_finfo->size_bytes;
    /* How many DPs is needed to write the data? */
    if (0 == ((num_chars - num_chars_left_first_dp) % NUM_BASIC_BYTES_DATA_PAGE))
    {
        num_accupied_dp = num_chars / NUM_BASIC_BYTES_DATA_PAGE;
    }
    else
    {
        num_accupied_dp = (num_chars / NUM_BASIC_BYTES_DATA_PAGE) + 1u;
    }
    /* Is there enough free space on Flash? */
    if (num_accupied_dp > pages_info.num_free_dp)
    {
        return FS_NO_MEMORY_LEFT;
    }

    uint32_t       curr_dp_log_addr = p_opened_file->dp_log_addr;
    fs_data_page_t data_page;
    /* Find the address of the last DP that contains file. */
    for (uint32_t cnt = 0; cnt < p_finfo->pages_occupied; ++cnt)
    {
        fs_err = tl_read_dp_flash(&data_page, curr_dp_log_addr,
                                  &wear_lvl, &pages_info);

        if (FS_ERR_OK != fs_err)
        {
            break;
        }

        if (0 == memcmp(data_page.extended_data.next_dp_addr, &last_dp_marker,
                        LUT_SIZE_BYTES))
        {
            break; /* This has been the last DP. */
        }

        memcpy(&curr_dp_log_addr, data_page.extended_data.next_dp_addr,
               LUT_SIZE_BYTES);
    }

    uint32_t free_dp_log_addr = 0;

    if (FS_ERR_OK == fs_err)
    {
        /* Get next free DP's logical address. */

        fs_err = tl_get_free_page_log_addr(&free_dp_log_addr,
                                           &wear_lvl, &pages_info, false, 0);
    }

    page_type_t page_type = OCCUPIED;

    if (FS_ERR_OK == fs_err)
    {
        /* Link current (last) DP to a new, free one. */
        memcpy(data_page.extended_data.next_dp_addr, &free_dp_log_addr,
               LUT_SIZE_BYTES);

        memcpy(&data_page.basic_data[data_page.extended_data.ending_byte + 1u],
               p_data, num_chars_left_first_dp);
        data_page.extended_data.ending_byte = NUM_BASIC_BYTES_DATA_PAGE - 1u;
        snorfs_attach_md5_dp(&data_page);

        if (opened_file.dp_log_addr == curr_dp_log_addr)
        {
            page_type = STARTING; /* This is a starting page. */
        }

        fs_err = tl_write_dp_flash(&data_page, curr_dp_log_addr, page_type,
                                   &wear_lvl, &pages_info);

    }
    /* Repeat until done. */
    if (FS_ERR_OK == fs_err)
    {
        page_type               = OCCUPIED;
        curr_dp_log_addr        = free_dp_log_addr;
        uint32_t num_chars_left = num_chars - num_chars_left_first_dp;
        /* First DP has been written. */
        for (uint32_t cnt = 0; cnt < num_accupied_dp - 1u; ++cnt)
        {
            memset(&data_page, 0xff, sizeof(data_page));
            fs_err = tl_get_free_page_log_addr(&free_dp_log_addr,
                                               &wear_lvl, &pages_info, false, 0);

            if (FS_ERR_OK != fs_err)
            {
                break;
            }

            if (NUM_BASIC_BYTES_DATA_PAGE >= num_chars_left)
            {
                /* Remaining data fits in this (last) DP. */
                if (0 == cnt)
                {
                    /* Only two DPs needed. */
                    memcpy(data_page.basic_data,
                           &p_data[num_chars_left_first_dp], num_chars_left);
                }
                else
                {
                    memcpy(data_page.basic_data,
                           &p_data[num_chars_left_first_dp +
                                   (NUM_BASIC_BYTES_DATA_PAGE * cnt)],
                           num_chars_left);
                }
                /* Cast valid - value can't exceed 511. */
                data_page.extended_data.ending_byte = (uint16_t)
                        (num_chars_left - 1u);
                snorfs_attach_md5_dp(&data_page);
            }
            else
            {
                /* Copy the 512 bytes. */
                memcpy(data_page.basic_data,
                       &p_data[num_chars_left_first_dp +
                               (NUM_BASIC_BYTES_DATA_PAGE * cnt)],
                       NUM_BASIC_BYTES_DATA_PAGE);
                /* Link this DP to the next one (logical address). */
                memcpy(data_page.extended_data.next_dp_addr,
                       &free_dp_log_addr, LUT_SIZE_BYTES);
                snorfs_attach_md5_dp(&data_page);

                num_chars_left -= NUM_BASIC_BYTES_DATA_PAGE;
            }

            fs_err = tl_write_dp_flash(&data_page, curr_dp_log_addr, page_type,
                                       &wear_lvl, &pages_info);

            if (FS_ERR_OK != fs_err)
            {
                break;
            }

            curr_dp_log_addr = free_dp_log_addr;
        }
    }

    return fs_err;
}

//--------------------------- INTERRUPT HANDLERS ------------------------------
