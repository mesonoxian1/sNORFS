/** @file snorfs_low_lvl.c
*
* @brief A description of the module's purpose.
*
* @par
* COPYRIGHT NOTICE: (c) 2018 Mihael Varga
* All rights reserved.
*/

//------------------------------ INCLUDES -------------------------------------
#include <snorfs_low_lvl.h>
#include <stdbool.h>
#include <blflash.h>
#include <blflash-info.h>
#include <RTT.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include "snorfs_logic_lyr.h"
#include "md5.h"
#include "snorfs_transition_lyr.h"

//-------------------------------- MACROS -------------------------------------
#define BYTES_IN_PAGE       (528u)
#define MAX_NUM_WRITE_TRIES (4u)
//----------------------------- DATA TYPES ------------------------------------

//--------------------- PRIVATE FUNCTION PROTOTYPES ---------------------------

//----------------------- STATIC DATA & CONSTANTS -----------------------------

//------------------------------ GLOBAL DATA ----------------------------------

//---------------------------- PUBLIC FUNCTIONS -------------------------------
bool fs_low_lvl_read_page (blflash_t *p_flash,
                           size_t page_phy_addr, void *p_data)
{
    bool b_success = true;

    blflash_error_t err;
    err = blflash_read(p_flash, page_phy_addr * BYTES_IN_PAGE, // Modify here.
                       p_data, 1 * BYTES_IN_PAGE);

    if (BLFLASH_ERROR_OK != err)
    {
        b_success = false;
    }

    return b_success;
}

bool fs_low_lvl_read_dp (blflash_t *p_flash,
                         size_t page_phy_addr, void *p_data)
{
    if (false == g_b_file_search)
    {
        dprintf (" Reading Data page from PHY location: %d\n", page_phy_addr);
    }

    return fs_low_lvl_read_page(p_flash, page_phy_addr, p_data);
}

bool fs_low_lvl_read_extended_data(blflash_t *p_flash,
                                   size_t *p_page_phy_addr, void *p_data)
{
    bool            b_success = true;
    blflash_error_t err;
    static fs_partition_page_t temp_pp_;
    /* Read only extended data - 16 bytes of it. */
    err = blflash_read(p_flash, (*p_page_phy_addr) * BYTES_IN_PAGE, // Modify here.
                       &temp_pp_, sizeof(temp_pp_));

    if (BLFLASH_ERROR_OK != err)
    {
        b_success = false;
    }

    memcpy(p_data, &temp_pp_.extended_data, sizeof(pp_extended_data_t));

    return b_success;
}

bool fs_low_lvl_read_dp_info (blflash_t *p_flash, size_t page_phy_addr,
                              uint16_t lut_pos, void *p_data)
{
    bool            b_success = true;
    blflash_error_t err;
    /* Read only pp_page_info_t data - 7 bytes of it. */
    fs_partition_page_t temp_pp2;

    err = blflash_read(p_flash,                      // Modify here.
                       page_phy_addr * BYTES_IN_PAGE,
                       &temp_pp2, sizeof(temp_pp2));

    memcpy(p_data, &temp_pp2.basic_data[lut_pos], sizeof(pp_page_info_t));

    if (BLFLASH_ERROR_OK != err)
    {
        b_success = false;
    }

    return b_success;
}

bool fs_low_lvl_write_page (blflash_t *p_flash,
                            size_t page_phy_addr, const void *p_data,
                            uint8_t num_tries)
{
    if (MAX_NUM_WRITE_TRIES < num_tries)
    {
        /* If the DP hasn't been correctly written in 5 tries, Flash is
         * probably corrupted. */
        return false;
    }

    xSemaphoreTakeRecursive(flash_write_semaphore, portMAX_DELAY);
    bool b_success = true;

    blflash_error_t    err;
    blflash_info_t     info;

    err = blflash_get_info(p_flash, &info);

    dprintf (" Writing page to PHY location: %d\n", page_phy_addr);

    if (BLFLASH_ERROR_OK == err)
    {
        /* Always read only one page. */
        err = blflash_write(p_flash, page_phy_addr * BYTES_IN_PAGE,  // Modify here.
                            p_data, 1 * BYTES_IN_PAGE);
    }

    if (BLFLASH_ERROR_OK != err)
    {
        b_success = false;
    }
    /* Re-read page and check MD5 hash. */
    if (true == b_success)
    {
        uint8_t temp_hash[PP_MD5_BYTES_PP];
        memset(temp_hash, 0xff, sizeof(temp_hash));
        /* Is this a DP or PP? */
        if ((0 == memcmp(starting_partition_name, p_data + 512,
                         strlen(starting_partition_name))) ||
            (0 == memcmp(random_partition_name, p_data + 512,
                         strlen(random_partition_name))))
        {
            /* This is PP. */
            calc_md5(p_data, sizeof(fs_partition_page_t) - PP_MD5_BYTES_PP,
                     temp_hash);

            static fs_partition_page_t temp_pp;
            err = blflash_read(p_flash, page_phy_addr * BYTES_IN_PAGE,
                               &temp_pp, 1 * BYTES_IN_PAGE);

            if (BLFLASH_ERROR_OK != err)
            {
                b_success = false;
            }
            else
            {
                if (0 != memcmp(temp_hash, temp_pp.extended_data.md5,
                                PP_MD5_BYTES_DP))
                {
                    dprintf("\tFailed to write PP! MD5 does not match!\n");
                    fs_low_lvl_flash_erase(p_flash, page_phy_addr, 1);
                    vTaskDelay(2 / portTICK_PERIOD_MS);
                    fs_low_lvl_write_page(p_flash, page_phy_addr, p_data,
                                          ++num_tries);
                    dprintf(" PP re-written\n");
                }
            }
        }
        else
        {
            /* This is DP. */
            calc_md5((uint8_t *) p_data,
                     sizeof(fs_partition_page_t) - PP_MD5_BYTES_PP, temp_hash);

            static fs_data_page_t temp_dp;
            err = blflash_read(p_flash, page_phy_addr * BYTES_IN_PAGE,
                               &temp_dp, 1 * BYTES_IN_PAGE);

            if (BLFLASH_ERROR_OK != err)
            {
                b_success = false;
            }
            else
            {
                if (0 != memcmp(temp_hash, temp_dp.extended_data.md5,
                                PP_MD5_BYTES_DP))
                {
                    dprintf("\tFailed to write DP! MD5 does not match!\n");
                    fs_low_lvl_flash_erase(p_flash, page_phy_addr, 1);
                    vTaskDelay(2 / portTICK_PERIOD_MS);
                    fs_low_lvl_write_page(p_flash, page_phy_addr, p_data,
                                          ++num_tries);
                    dprintf(" DP re-written\n");
                }
            }
        }
    }

    xSemaphoreGiveRecursive(flash_write_semaphore);
    return b_success;
}

bool fs_low_lvl_flash_erase (blflash_t *p_flash,
                             size_t page_phy_addr, size_t num_pages)
{
    bool b_success = true;

    blflash_error_t err;
    blflash_info_t  info;

    err = blflash_get_info(p_flash, &info);

    if (BLFLASH_ERROR_OK == err)
    {
        err = blflash_erase(p_flash,                      // Modify here.
                            page_phy_addr * BYTES_IN_PAGE,
                            num_pages * BYTES_IN_PAGE);
    }

    if (BLFLASH_ERROR_OK != err)
    {
        b_success = false;
    }

    return b_success;
}
//--------------------------- PRIVATE FUNCTIONS -------------------------------

//--------------------------- INTERRUPT HANDLERS ------------------------------
