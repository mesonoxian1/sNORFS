/** @file snorfs_transition_lyr.h
*
* @brief See source file.
*
* @par
* COPYRIGHT NOTICE: (c) 2018 Mihael Varga
* All rights reserved.
*/

#ifndef MY_AWESOME_DIPLOMSKI_MY_FS_TRANSITION_LYR_H
#define MY_AWESOME_DIPLOMSKI_MY_FS_TRANSITION_LYR_H

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------ INCLUDES -------------------------------------
#include <stdint.h>
#include <stdbool.h>
#include <sched.h>
#include <blflash.h>
#include <FreeRTOS.h>
#include <semphr.h>
#include "snorfs_logic_lyr.h"
//-------------------------- CONSTANTS & MACROS -------------------------------
/* Change this value if using Flash with total page number bigger then 4096.
 * The number is result of: (page number / 74) + 1; e.g. (4096 / 74) + 1 = 56.*/
#define MAX_NUM_PP_AT45 (56u)
//----------------------------- DATA TYPES ------------------------------------
/** Contains data for Wear leveling.*/
typedef struct
{
    uint32_t value_most_erased_dp;
    uint32_t value_least_erased_dp;
    uint32_t log_addr_last_used_dp;
    uint32_t log_addr_most_worn_dp;
} wear_lvl_t;

/** Contains data about FS pages. */
typedef struct
{
    uint32_t starting_pp_phy_addr;
    uint32_t num_pp;
    uint32_t num_dp;
    uint32_t num_free_dp;
    fs_lut   addr_pp[MAX_NUM_PP_AT45];
} pages_info_t;

extern SemaphoreHandle_t flash_read_semaphore;
extern SemaphoreHandle_t flash_write_semaphore;
//---------------------- PUBLIC FUNCTION PROTOTYPES ---------------------------
/**@brief  Writes a DP to a flash based on it's Logical address.
 *
 * @param  p_data_page[in]     - Pointer to prepared DP to write.
 * @param  log_dp_location[in] - Logical address of DP to write.
 * @param  p_wear_lvl[in]      - Pointer to wear leveling structure.
 * @param  p_page_info[in]     - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
fs_err_code_t tl_write_dp_flash (fs_data_page_t *p_data_page,
                                 uint32_t log_dp_location,
                                 page_type_t page_type,
                                 wear_lvl_t *p_wear_lvl,
                                 pages_info_t *p_page_info);

/**@brief  Reads a DP from a flash based on it's Logical address.
 *
 * @param  p_data_page[out]    - Pointer to storing place (528 bytes).
 * @param  log_dp_location[in] - Logical address of DP to read.
 * @param  p_wear_lvl[in]      - Pointer to wear leveling structure.
 * @param  p_page_info[in]     - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
fs_err_code_t tl_read_dp_flash (fs_data_page_t *p_data_page,
                                uint32_t log_dp_location,
                                wear_lvl_t *p_wear_lvl,
                                pages_info_t *p_page_info);

/**@brief  Deletes a DP from Flash based on it's Logical address and updates
 *         PP containing data about that DP.
 * @note   This function won't wear level erased DP and asocciated PP, that
 *         will be hanled later during some write procedure.
 *
 * @param  log_dp_location[in] - Logical addres of DP to delete.
 * @param  p_wear_lvl[in]      - Pointer to wear leveling structure.
 * @param  p_page_info[in]     - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
fs_err_code_t tl_erase_dp_flash (uint32_t log_dp_location,
                                 wear_lvl_t *p_wear_lvl,
                                 pages_info_t *p_page_info);

/**@brief  Reads a PP from a flash based on it's Logical address.
 *
 * @param  p_partition_page[out] - Pointer to storing place (528 bytes).
 * @param  log_pp_location[in]   - Logical address of the PP.
 * @param  p_wear_lvl[in]        - Pointer to wear leveling structure.
 * @param  p_page_info[in]       - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
fs_err_code_t tl_read_pp_flash (fs_partition_page_t *p_partition_page,
                                uint32_t log_pp_location,
                                wear_lvl_t *p_wear_lvl,
                                pages_info_t *p_page_info);

/**@brief Get Logical address of a free page.
 *
 * @param p_free_dp_log_addr[out] - Pointer to free DP's address storing loc.
 * @param p_wear_lvl[in]          - Pointer to wear leveling structure.
 * @param p_page_info[in]         - Pointer to Info structure.
 * @param continuous_search[in]   - If this is false, logical address of a
 *                                  first free DP will be found. If true, every
 *                                  next iteration will get a new address.
 * @param dp_addr_to_avoid[in]    - Find any free DP but this value. If in
 *                                  continues mode, this value is ignored.
 *
 * @return FS_ERR_OK if ok, err code otherwise.
 */
fs_err_code_t tl_get_free_page_log_addr (uint32_t *p_free_dp_log_addr,
                                         wear_lvl_t *p_wear_lvl,
                                         pages_info_t *p_page_info,
                                         bool continuous_search,
                                         uint32_t dp_addr_to_avoid);

/**@brief  Setup p_wear_lvl's most and least erased DP values.
 *
 * @param  p_wear_lvl[in]  - Pointer to wear leveling structure.
 * @param  p_page_info[in] - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
fs_err_code_t tl_find_least_and_most_worn_value (wear_lvl_t *p_wear_lvl,
                                                 pages_info_t *p_page_info);

#ifdef __cplusplus
}
#endif

#endif //MY_AWESOME_DIPLOMSKI_MY_FS_TRANSITION_LYR_H
