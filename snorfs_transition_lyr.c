/** @file snorfs_transition_lyr.c
*
* @brief A transition layer for wear leveling of sNORFS. This layer handles
 *       logical addresses, translates them to the physical ones and takes care
 *       of static and dynamic wear leveling.
*
* @par
* COPYRIGHT NOTICE: (c) 2018 Mihael Varga
* All rights reserved.
*/

//------------------------------ INCLUDES -------------------------------------
#include <snorfs_low_lvl.h>
#include <stdbool.h>
#include <blflash.h>
#include <blflash-info.h>
#include <snorfs_transition_lyr.h>
#include <string.h>
#include <FreeRTOS.h>
#include <semphr.h>

#include "RTT.h"
#include "SEGGER_RTT.h"
#include "snorfs_transition_lyr.h"
#include "snorfs_logic_lyr.h"
//-------------------------------- MACROS -------------------------------------
#define ALLOWED_ERASE_CNT_PP_OVERVALUE    (300u)
#define STATIC_WEAR_LVL_UPPER_ERASE_LIMIT (500u)
#define TL_DEBUG                          (false) /* Enable for RTT. */
//----------------------------- DATA TYPES ------------------------------------

//--------------------- PRIVATE FUNCTION PROTOTYPES ---------------------------
/**@brief  Find the least worn out DP. This function is normally called when
 *         exchanging most worn out DP or PP with the one found using this.
 *
 * @param  p_log_addr_least_worn_page[out] - Pointer to storing location for
 *                                           Logical address of found DP.
 * @param  p_wear_lvl[in,out]              - Pointer to wear_lvl_t structure.
 * @param  p_page_info[in]                 - Pointer to Info structure.
 * @param  b_force[in]                     - Used at mount to scan whole Flash.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static fs_err_code_t tl_find_least_worn_dp (uint32_t *p_log_addr_least_worn_page,
                                            wear_lvl_t *p_wear_lvl,
                                            pages_info_t *p_page_info,
                                            bool b_force);
/**@brief  Find the most worn out DP. This function is called when
 *         mounting FS to find out the value ofmost worn out DP.
 *
 * @param  p_log_addr_most_worn_page[out] - Pointer to storing location for
 *                                          Logical address of found DP.
 * @param  p_wear_lvl[in,out]             - Pointer to wear_lvl_t structure.
 * @param  p_page_info[in]                - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static fs_err_code_t tl_find_most_worn_dp (uint32_t *p_log_addr_most_worn_page,
                                           wear_lvl_t *p_wear_lvl,
                                           pages_info_t *p_page_info);

static fs_err_code_t tl_find_least_worn_dp_inversed(
        uint32_t *p_log_addr_least_worn_page,
        wear_lvl_t *p_wear_lvl,
        pages_info_t *p_page_info);

 /**@brief  Writes DP using wear leveling algorithm. Finds the next free
  *         page, exchanges these two DPs, updates PP (one or two), depending
  *         if exchanged DPs were contained in the same or different PPs.
  *         Then do a check if PP is to much worn out, do a static wear
  *         leveling. At the end check if current DP erase count is too big
  *         (DP is worn out compared to least worn out DP); if so, do a
  *         static wear leveling of that DP.
  *
  * @param  p_data_page[in]    - Pointer to DP to write.
  * @param  phy_dp_addr[in]    - Physical address of DP on Flash.
  * @param  log_dp_addr[in]    - Logical address of DP.
  * @param  phy_pp_addr[in]    - Physical address of PP containing DP's data.
  * @param  num_of_pp[in]      - Number of PP that contains data.
  * @param  dp_lut_pos[in]     - Position of LUT inside a PP concerning this DP.
  * @param  p_wear_lvl[in,out] - Pointer to wear_lvl_t structure.
  * @param  p_page_info[in]    - Pointer to Info structure.
  * @return FS_ERR_OK if ok, err code otherwise.
  */
static fs_err_code_t tl_write_page_wear_lvl (
        fs_data_page_t *p_data_page,
        uint32_t phy_dp_addr, uint32_t log_dp_addr,
        uint32_t phy_pp_addr, uint32_t log_pp_addr,
        uint32_t num_of_pp, uint16_t dp_lut_pos,
        page_type_t page_type,
        wear_lvl_t *p_wear_lvl, pages_info_t *p_page_info);

/**@brief  Get physical address of PP that contains that DP. Can also be used
 *         to get physical address of PP if logical PP address is passed.
 *
 * @param  log_addr_dp[in]    - Logical address od DP (or PP - divison '/'
 *                              with 74).
 * @param  p_phy_pp_addr[out] - Pointer to Physical address's storing location.
 * @param  p_page_info[in]    - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static fs_err_code_t tl_get_phy_addr_pp (uint32_t log_addr_dp,
                                         uint32_t *p_phy_pp_addr,
                                         pages_info_t *p_page_info,
                                         bool b_fast_search);

/**@brief  Get physical address of a DP based on DP's logical address.
 * 
 * @param  log_addr_dp[in]    - Logical address od DP, musn't be a multiple of
 *                              74 - those Phyisical addresess are reserved
 *                              for PPs.
 * @param  p_phy_dp_addr[out] - Pointer to physical address's storing location.
 * @param  p_page_info[in]    - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static fs_err_code_t tl_get_phy_addr_dp (uint32_t log_addr_dp,
                                         uint32_t *p_phy_dp_addr,
                                         pages_info_t *p_page_info);

/**@brief  Increments ECT value of DP located in PP by one.
 *
 * @param  dp_ect_pos[in] - Location of DP's data inside PP's basic data part.
 * @param  p_temp_pp[in]  - Pointer to PP containing DP.
 * @param  p_wear_lvl[in] - Pointer to wear leveling structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static void tl_update_ect_dp (uint16_t dp_ect_pos,
                              fs_partition_page_t *p_temp_pp,
                              wear_lvl_t *p_wear_lvl);

/**@brief  Updates LUT of DP contained in PP.
 *
 * @param  dp_lut_pos
 * @param  p_temp_pp
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static void tl_update_lut_dp (uint16_t dp_lut_pos, uint32_t *p_new_dp_phy_addr,
                              fs_partition_page_t *p_temp_pp);

/**@brief Increments Erase count of given PP by one.
 *
 * @param p_curr_partition_page[in] - Pointer to PP.
 */
static void tl_update_ect_pp (fs_partition_page_t *p_partition_page);

/**@brief  Gets erase count of a DP based on its logical address.
 *
 * @param  p_erase_cnt[out] - Pointer to storing location of DP's Erase count.
 * @param  dp_log_addr[in]  - Logical address of DP.
 * @param  p_page_info[in]    - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static fs_err_code_t tl_get_erase_cnt_dp (uint32_t *p_erase_cnt,
                                          uint32_t dp_log_addr,
                                          pages_info_t *p_page_info);

/**@brief  Checks if Erase count of PP exceeds value of (least erased DP +
 *         ALLOWED_ERASE_CNT_PP_OVERVALUE). If so, the least worn out DP is
 *         found and exchanged with given PP, making PP less worn out, and DP
 *         with low usage (static data) more worn out. This evens the wear.
 *
 * @param  p_partition_page[in] - Pointer to PP of interest.
 * @param  pp_phy_addr[in]      - Physical address of that PP.
 * @param  p_wear_lvl[in]       - Pointer to wear leveling structure.
 * @param  p_page_info[in]      - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static fs_err_code_t tl_check_and_wear_lvl_pp (
                            fs_partition_page_t *p_partition_page,
                            uint32_t pp_phy_addr, uint32_t pp_log_addr,
                            wear_lvl_t *p_wear_lvl, pages_info_t *p_page_info);

/**@brief  Finds physical address of PP(2) pointing to this PP(1), since they
 *         are chain linked.
 *
 * @param  p_pp_phy_addr[in]                - Pointer to physical address of
 *                                            PP(1) for which pointing PP(2)
 *                                            must be found.
 * @param  p_pp_containing_pp_phy_addr[out] - Pointer to Physical address of PP
 *                                            (2) that points to first one.
 * @param  p_page_info[in]                  - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
fs_err_code_t tl_find_pp_containing_pp (uint32_t *p_pp_phy_addr,
                                        uint32_t *p_pp_containing_pp_phy_addr,
                                        pages_info_t *p_page_info);

/**@brief  Finds least worn DP and changes that DP with the given one (most
 *         worn out page).
 *
 * @param  log_addr_most_worn_dp[in] - Logical address of most worn out DP.
 * @param  p_partition_page[in]      - Pointer to PP containing that DP's data.
 * @param  p_wear_lvl[in]            - Pointer to wear leveling structure.
 * @param  p_page_info[in]           - Pointer to Info structure.
 * @return FS_ERR_OK if ok, err code otherwise.
 */
static fs_err_code_t tl_exchange_dp (uint32_t log_addr_most_worn_dp,
                                     fs_partition_page_t *p_partition_page,
                                     wear_lvl_t *p_wear_lvl,
                                     pages_info_t *p_page_info);
//----------------------- STATIC DATA & CONSTANTS -----------------------------
static const fs_lut last_dp_marker = {{0xff, 0xff, 0xff}};
//------------------------------ GLOBAL DATA ----------------------------------
SemaphoreHandle_t flash_read_semaphore;
SemaphoreHandle_t flash_write_semaphore;
//---------------------------- PUBLIC FUNCTIONS -------------------------------
fs_err_code_t tl_write_dp_flash(fs_data_page_t *p_data_page,
                                uint32_t log_dp_location,
                                page_type_t page_type,
                                wear_lvl_t *p_wear_lvl,
                                pages_info_t *p_page_info)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    /* Sanity check. */
    blflash_info_t flash_info;
    blflash_error_t flash_err = blflash_get_info(p_fs_flash, &flash_info);

    if (BLFLASH_ERROR_OK != flash_err)
    {
        dprintf("Flash error during getting info\n");
        fs_err = FS_FLASH_HW_ERR;
    }

    if (log_dp_location >= flash_info.num_pages)
    {
        dprintf("Flash error: unsupported page address\n");
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (0 == (log_dp_location % (NUM_LUTS_IN_PP +1u)))
    {
        /* These logical adresses are reserved for PPs! */
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Get the physical address of the DP. */
        uint32_t dp_phy_loc = 0;
        fs_err = tl_get_phy_addr_dp(log_dp_location, &dp_phy_loc, p_page_info);

        if (FS_ERR_OK != fs_err)
        {
            return fs_err;
        }
        /* Get the physical address of the PP pointing to this DP. */
        uint32_t pp_phy_loc = 0;
        fs_err = tl_get_phy_addr_pp(log_dp_location, &pp_phy_loc,
                                    p_page_info, true);

        if (FS_ERR_OK != fs_err)
        {
            return fs_err;
        }
        /* Cast needed because function type, value won't exceed 72. */
        uint16_t lut_pos = (uint16_t)
                           ((log_dp_location % (NUM_LUTS_IN_PP + 1u)) - 1u);

        uint32_t num_of_pp = log_dp_location / (NUM_LUTS_IN_PP + 1u);

        uint32_t log_pp_addr = log_dp_location / (NUM_LUTS_IN_PP + 1u);
        log_pp_addr         *= (NUM_LUTS_IN_PP + 1u);
        /* Burn the DP. */
        fs_err = tl_write_page_wear_lvl(p_data_page,
                                        dp_phy_loc, log_dp_location,
                                        pp_phy_loc, log_pp_addr,
                                        num_of_pp, lut_pos, page_type,
                                        p_wear_lvl, p_page_info);

        if (FS_ERR_OK != fs_err)
        {
            dprintf("Wear leveled writing failed!\n");
        }
    }

    return fs_err;
}

fs_err_code_t tl_read_dp_flash(fs_data_page_t *p_data_page,
                             uint32_t log_dp_location,
                             wear_lvl_t *p_wear_lvl,
                             pages_info_t *p_page_info)
{
    xSemaphoreTake(flash_read_semaphore, portMAX_DELAY);
    (void) p_wear_lvl;
    fs_err_code_t fs_err = FS_ERR_OK;
    /* Sanity check. */
    blflash_info_t flash_info;
    blflash_error_t flash_err = blflash_get_info(p_fs_flash, &flash_info);

    if (BLFLASH_ERROR_OK != flash_err)
    {
        dprintf("Flash error during getting info\n");
        fs_err = FS_FLASH_HW_ERR;
    }

    if (log_dp_location >= flash_info.num_pages)
    {
        dprintf("Flash error: unsupported page address\n");
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (0 == (log_dp_location % (NUM_LUTS_IN_PP +1u)))
    {
        /* These logical adresses are reserved for PPs! */
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Get the physical address of the DP. */
        uint32_t dp_phy_loc = 0;
        fs_err = tl_get_phy_addr_dp(log_dp_location, &dp_phy_loc, p_page_info);

        if (FS_ERR_OK != fs_err)
        {
            return fs_err;
        }
        /* Read the DP. */
        bool b_ret = fs_low_lvl_read_dp(p_fs_flash, dp_phy_loc, p_data_page);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_READ_ERR;
        }
    }

    xSemaphoreGive(flash_read_semaphore);
    return fs_err;
}

fs_err_code_t tl_erase_dp_flash(uint32_t log_dp_location,
                              wear_lvl_t *p_wear_lvl,
                              pages_info_t *p_page_info)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    /* Sanity check. */
    blflash_info_t flash_info;
    blflash_error_t flash_err = blflash_get_info(p_fs_flash, &flash_info);

    if (BLFLASH_ERROR_OK != flash_err)
    {
        dprintf("Flash error during getting info\n");
        fs_err = FS_FLASH_HW_ERR;
    }

    if (log_dp_location >= flash_info.num_pages)
    {
        dprintf("Flash error: unsupported page address\n");
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (0 == (log_dp_location % (NUM_LUTS_IN_PP + 1u)))
    {
        /* These logical addresses are reserved for PPs! */
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Get the physical address of the DP. */
        uint32_t dp_phy_addr = 0;
        fs_err = tl_get_phy_addr_dp(log_dp_location, &dp_phy_addr, p_page_info);

        if (FS_ERR_OK != fs_err)
        {
            return fs_err;
        }
        /** Erase the DP! */
        bool b_ret = fs_low_lvl_flash_erase(p_fs_flash, dp_phy_addr, 1);

        if (false == b_ret)
        {
            return FS_FLASH_ERASE_ERR;
        }
        /* Update the ECT of the PP pointing to this DP. Wear leveling will
         * be done later when data will be written to the Flash. */
        fs_partition_page_t partition_page;
        /* Get the logical address of the PP. */
        uint32_t log_pp_addr = (log_dp_location / (NUM_LUTS_IN_PP + 1u)) *
                               (NUM_LUTS_IN_PP + 1u);

        fs_err = tl_read_pp_flash(&partition_page, log_pp_addr,
                                  p_wear_lvl, p_page_info);

        if (FS_ERR_OK == fs_err)
        {
            /* Cast needed because function type, value won't exceed 72. */
            uint16_t ect_pos = (uint16_t)
                    ((log_dp_location % (NUM_LUTS_IN_PP + 1u)) - 1u);

            tl_update_ect_dp(ect_pos, &partition_page, p_wear_lvl);
            partition_page.basic_data[ect_pos].page_type = FREE;
            tl_update_ect_pp(&partition_page);
            snorfs_attach_md5_pp(&partition_page);

            uint32_t phy_addr_pp = 0;
            fs_err = tl_get_phy_addr_pp(log_dp_location, &phy_addr_pp,
                                        p_page_info, true);

            b_ret = fs_low_lvl_write_page(p_fs_flash,
                                          phy_addr_pp, &partition_page, 0);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }

        if (FS_ERR_OK != fs_err)
        {
            dprintf("Wear leveled writing failed!\n");
        }
        else
        {
            p_page_info->num_free_dp++;
        }
    }

    return fs_err;
}

fs_err_code_t tl_read_pp_flash(fs_partition_page_t *p_partition_page,
                             uint32_t log_pp_location,
                             wear_lvl_t *p_wear_lvl,
                             pages_info_t *p_page_info)
{
    xSemaphoreTake(flash_read_semaphore, portMAX_DELAY);
    (void) p_wear_lvl;
    fs_err_code_t fs_err = FS_ERR_OK;
    /* Sanity check. */
    blflash_info_t flash_info;
    blflash_error_t flash_err = blflash_get_info(p_fs_flash, &flash_info);

    if (BLFLASH_ERROR_OK != flash_err)
    {
        dprintf("Flash error during getting info\n");
        fs_err = FS_FLASH_HW_ERR;
    }

    if (log_pp_location >= flash_info.num_pages)
    {
        dprintf("Flash error: unsupported page address\n");
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (0 != (log_pp_location % (NUM_LUTS_IN_PP + 1u)))
    {
        /* These logical addresses are reserved for DPs! */
        fs_err = FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Get the physical address of the PP. */
        uint32_t pp_phy_loc = 0;
        fs_err = tl_get_phy_addr_pp(log_pp_location, &pp_phy_loc,
                                    p_page_info, true);

        if (FS_ERR_OK != fs_err)
        {
            return fs_err;
        }
        /* Read the DP. */
        bool b_ret = fs_low_lvl_read_page(p_fs_flash, pp_phy_loc,
                                          p_partition_page);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_READ_ERR;
        }
    }

    xSemaphoreGive(flash_read_semaphore);
    return fs_err;
}

static void tl_update_ect_dp(uint16_t dp_ect_pos,
                             fs_partition_page_t *p_temp_pp,
                             wear_lvl_t *p_wear_lvl)
{
    uint32_t erase_cnt = 0;

    memcpy(&erase_cnt, &p_temp_pp->basic_data[dp_ect_pos].ect, ECT_SIZE_BYTES);
    erase_cnt++;
    memcpy(&p_temp_pp->basic_data[dp_ect_pos].ect, &erase_cnt, ECT_SIZE_BYTES);

    /* If erase_cnt value is larger than value of erase count on most worn
     * out page, this is now that page. */
    if (erase_cnt > p_wear_lvl->value_most_erased_dp)
    {
        p_wear_lvl->value_most_erased_dp = erase_cnt;
    }
}

fs_err_code_t tl_get_free_page_log_addr(uint32_t *p_free_dp_log_addr,
                                      wear_lvl_t *p_wear_lvl,
                                      pages_info_t *p_page_info,
                                      bool continuous_search,
                                      uint32_t dp_addr_to_avoid)
{
    static uint32_t previous_dp_log_addr = 0;
    static uint32_t previous_pp_cnt = 0;

    fs_err_code_t       fs_err;
    fs_partition_page_t temp_pp;
    int32_t             cnt_pp = 0;
    bool                b_already_searched = false;

    if (true == continuous_search)
    {
        if (cnt_pp >= p_page_info->num_pp)
        {
            cnt_pp = 0;
        }
        else
        {
            /* Otherwise just continue from last cnt_pp value. */
            cnt_pp = previous_pp_cnt;
        }
    }
    else
    {
        cnt_pp = 0;
    }
    /* Search for first free location inside Flash. */
    for (; cnt_pp < p_page_info->num_pp; ++cnt_pp)
    {
        /* Search every 74th logical position. */
        fs_err = tl_read_pp_flash(&temp_pp,
                                  cnt_pp * (NUM_LUTS_IN_PP + 1),
                                  p_wear_lvl, p_page_info);

        if (FS_ERR_OK != fs_err)
        {
            return fs_err;
        }

        if (p_page_info->num_free_dp <= 10)
        {
            dprintf("\n");
        }

        for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
        {
            if (p_page_info->num_free_dp <= 10)
            {
                dprintf("%d ", temp_pp.basic_data[lut_cnt].page_type);
            }

            if (FREE == temp_pp.basic_data[lut_cnt].page_type)
            {
                /* This is the Logical location of the first free page. */
                *p_free_dp_log_addr = cnt_pp * (NUM_LUTS_IN_PP + 1u) +
                                      lut_cnt + 1u;

                if (*p_free_dp_log_addr >=
                    (p_page_info->num_dp + p_page_info->num_pp))
                {
                    /* If no more DPs are available. */
                    *p_free_dp_log_addr  = previous_dp_log_addr;
                    previous_dp_log_addr = 0;
                    previous_pp_cnt      = 0;
                    return FS_ERR_OK;
                }

                if (false == continuous_search)
                {
                    /* First free DP is O.K. */
                    if (*p_free_dp_log_addr != dp_addr_to_avoid)
                    {
                        return FS_ERR_OK;
                    }
                }
                else
                {
                    /* Otherwise find new free DP. Only search new logical
                     * addresses. */
                    if (*p_free_dp_log_addr > previous_dp_log_addr)
                    {
                        /* This is the free DP different from last one. */
                        previous_dp_log_addr = *p_free_dp_log_addr;
                        /* Legit cast, value won't be negative in this point. */
                        previous_pp_cnt      = (uint32_t) cnt_pp;
                        return FS_ERR_OK;
                    }
                }
            }
            /* If no free page has been found and last page has been read. */
            if ((cnt_pp == (p_page_info->num_pp - 1u) &&
                 (lut_cnt == (NUM_LUTS_IN_PP - 1u))))
            {
                if (true == b_already_searched)
                {
                    return FS_NO_MEMORY_LEFT;
                }

                cnt_pp               = -1;
                previous_dp_log_addr = 0;
                b_already_searched   = true;
            }
        }
    }
    /* The loop should've returned the location, if not, that means that no
     * free pages remain in Flash. */
    fs_err               = FS_NO_MEMORY_LEFT;
    previous_dp_log_addr = 0;
    return fs_err;
}

fs_err_code_t tl_find_least_and_most_worn_value(wear_lvl_t *p_wear_lvl,
                                                pages_info_t *p_page_info)
{
    fs_err_code_t fs_err;
    /* Set the p_wear_lvl->value_least_erased_page. */
    uint32_t dummy_log_addr_least_worn;
    fs_err = tl_find_least_worn_dp(&dummy_log_addr_least_worn,
                                   p_wear_lvl, p_page_info, true);

    if (FS_ERR_OK == fs_err)
    {
        /* Set the p_wear_lvl->value_most_erased_page. */
        uint32_t dummy_log_addr_most_worn;
        fs_err = tl_find_most_worn_dp(&dummy_log_addr_most_worn,
                                      p_wear_lvl, p_page_info);
    }

    return fs_err;
}
//--------------------------- PRIVATE FUNCTIONS -------------------------------
static fs_err_code_t tl_find_least_worn_dp(uint32_t *p_log_addr_least_worn_page,
                                           wear_lvl_t *p_wear_lvl,
                                           pages_info_t *p_page_info,
                                           bool b_force)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    uint32_t    max_worn_value = 0xffffffff; /* Start with maximal possible. */
    /* Search for the least worn out Page location inside Flash. */
    fs_partition_page_t temporary_pp;
    uint32_t            worn_value = 0;
    /* Decide the direction of binary search based on the last used least
     * worn DP's location. */
    if (p_wear_lvl->log_addr_last_used_dp <
                    ((p_page_info->num_pp + p_page_info->num_dp) / 2u))
    {
        for (uint32_t cnt_pp = 0; cnt_pp < p_page_info->num_pp; ++cnt_pp)
        {
            /* Search every 74th logical position. */
            fs_err = tl_read_pp_flash(&temporary_pp,
                                      cnt_pp * (NUM_LUTS_IN_PP + 1),
                                      p_wear_lvl, p_page_info);

            if (FS_ERR_OK != fs_err)
            {
                break;
            }

            for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
            {
                /* If last LUT has been reached. */
                if (0 == memcmp(&last_dp_marker,
                                &temporary_pp.basic_data[lut_cnt].lut,
                                LUT_SIZE_BYTES))
                {
                   break;
                }

                worn_value = 0;
                memcpy(&worn_value, &temporary_pp.basic_data[lut_cnt].ect,
                       ECT_SIZE_BYTES);

                if (worn_value < max_worn_value)
                {
                    max_worn_value = worn_value;
                    /* This is currently the least worn out location. */
                    *p_log_addr_least_worn_page =
                            cnt_pp * (NUM_LUTS_IN_PP + 1u) +
                            lut_cnt + 1u;

                    if ((worn_value == p_wear_lvl->value_least_erased_dp) &&
                        (false == b_force))
                    {
                        /* The first DP with ECT equal to least worn out one is
                         * returned. When all the DPs with ECT equal to the
                         * least erased DP are used, the loop won't return
                         * here and value of least erased DP will be updated. */
                        p_wear_lvl->log_addr_last_used_dp =
                                *p_log_addr_least_worn_page;
                        return FS_ERR_OK;
                    }
                }
            }
        }
    }
    else
    {
        for (int32_t cnt_pp = p_page_info->num_pp - 1u; cnt_pp >= 0; --cnt_pp)
        {
            /* Search every 74th logical position. */
            fs_err = tl_read_pp_flash(&temporary_pp,
                                      cnt_pp * (NUM_LUTS_IN_PP + 1),
                                      p_wear_lvl, p_page_info);

            if (FS_ERR_OK != fs_err)
            {
                break;
            }

            for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
            {
                /* If last LUT has been reached. */
                if (0 == memcmp(&last_dp_marker,
                                &temporary_pp.basic_data[lut_cnt].lut,
                                LUT_SIZE_BYTES))
                {
                    break;
                }

                worn_value = 0;
                memcpy(&worn_value, &temporary_pp.basic_data[lut_cnt].ect,
                       ECT_SIZE_BYTES);

                if (worn_value < max_worn_value)
                {
                    max_worn_value = worn_value;
                    /* This is currently the least worn out location. */
                    *p_log_addr_least_worn_page =
                            cnt_pp * (NUM_LUTS_IN_PP + 1u) +
                            lut_cnt + 1u;

                    if (worn_value == p_wear_lvl->value_least_erased_dp)
                    {
                        /* The first DP with ECT equal to least worn out one is
                         * returned. When all the DPs with ECT equal to the
                         * least erased DP are used, the loop won't return
                         * here and value of least erased DP will be updated. */
                        p_wear_lvl->log_addr_last_used_dp =
                                *p_log_addr_least_worn_page;
                        return FS_ERR_OK;
                    }
                }
            }
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        p_wear_lvl->value_least_erased_dp = max_worn_value;
        p_wear_lvl->log_addr_last_used_dp = *p_log_addr_least_worn_page;
    }

    return fs_err;
}

static fs_err_code_t tl_find_least_worn_dp_inversed(
        uint32_t *p_log_addr_least_worn_page,
        wear_lvl_t *p_wear_lvl,
        pages_info_t *p_page_info)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    uint32_t    max_worn_value = 0xffffffff; /* Start with maximal possible. */
    /* Search for the least worn out Page location inside Flash. */
    fs_partition_page_t temporary_pp;
    uint32_t            worn_value = 0;
    /* Decide the direction of binary search based on the last used least
     * worn DP's location. */
    if (p_wear_lvl->log_addr_last_used_dp >
        ((p_page_info->num_pp + p_page_info->num_dp) / 2u))
    {
        for (uint32_t cnt_pp = 0; cnt_pp < p_page_info->num_pp; ++cnt_pp)
        {
            /* Search every 74th logical position. */
            fs_err = tl_read_pp_flash(&temporary_pp,
                                      cnt_pp * (NUM_LUTS_IN_PP + 1),
                                      p_wear_lvl, p_page_info);

            if (FS_ERR_OK != fs_err)
            {
                break;
            }

            for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
            {
                /* If last LUT has been reached. */
                if (0 == memcmp(&last_dp_marker,
                                &temporary_pp.basic_data[lut_cnt].lut,
                                LUT_SIZE_BYTES))
                {
                    break;
                }

                worn_value = 0;
                memcpy(&worn_value, &temporary_pp.basic_data[lut_cnt].ect,
                       ECT_SIZE_BYTES);

                if (worn_value < max_worn_value)
                {
                    max_worn_value = worn_value;
                    /* This is currently the least worn out location. */
                    *p_log_addr_least_worn_page =
                            cnt_pp * (NUM_LUTS_IN_PP + 1u) +
                            lut_cnt + 1u;

                    if (worn_value == p_wear_lvl->value_least_erased_dp)
                    {
                        /* The first DP with ECT equal to least worn out one is
                         * returned. When all the DPs with ECT equal to the
                         * least erased DP are used, the loop won't return
                         * here and value of least erased DP will be updated. */
                        return FS_ERR_OK;
                    }
                }
            }
        }
    }
    else
    {
        for (int32_t cnt_pp = p_page_info->num_pp - 1u; cnt_pp >= 0; --cnt_pp)
        {
            /* Search every 74th logical position. */
            fs_err = tl_read_pp_flash(&temporary_pp,
                                      cnt_pp * (NUM_LUTS_IN_PP + 1),
                                      p_wear_lvl, p_page_info);

            if (FS_ERR_OK != fs_err)
            {
                break;
            }

            for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
            {
                /* If last LUT has been reached. */
                if (0 == memcmp(&last_dp_marker,
                                &temporary_pp.basic_data[lut_cnt].lut,
                                LUT_SIZE_BYTES))
                {
                    break;
                }

                worn_value = 0;
                memcpy(&worn_value, &temporary_pp.basic_data[lut_cnt].ect,
                       ECT_SIZE_BYTES);

                if (worn_value < max_worn_value)
                {
                    max_worn_value = worn_value;
                    /* This is currently the least worn out location. */
                    *p_log_addr_least_worn_page =
                            cnt_pp * (NUM_LUTS_IN_PP + 1u) +
                            lut_cnt + 1u;

                    if (worn_value == p_wear_lvl->value_least_erased_dp)
                    {
                        /* The first DP with ECT equal to least worn out one is
                         * returned. When all the DPs with ECT equal to the
                         * least erased DP are used, the loop won't return
                         * here and value of least erased DP will be updated. */
                        return FS_ERR_OK;
                    }
                }
            }
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        p_wear_lvl->value_least_erased_dp = worn_value;
        p_wear_lvl->log_addr_last_used_dp = *p_log_addr_least_worn_page;
    }

    return fs_err;
}


static fs_err_code_t tl_find_most_worn_dp(uint32_t *p_log_addr_most_worn_page,
                                        wear_lvl_t *p_wear_lvl,
                                        pages_info_t *p_page_info)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    uint32_t    min_worn_value = 0; /* Start with minimal possible. */
    bool        b_backup = false;
    /* Search for the most worn out DP location inside Flash. */
    fs_partition_page_t temporary_pp;

    for (uint16_t cnt_pp = 0; cnt_pp < p_page_info->num_pp; ++cnt_pp)
    {
        /* Search every 74th logical position. */
        fs_err = tl_read_pp_flash(&temporary_pp,
                                  cnt_pp * (NUM_LUTS_IN_PP + 1),
                                  p_wear_lvl, p_page_info);

        if (FS_ERR_OK != fs_err)
        {
            return FS_FLASH_READ_ERR;
        }

        if (false == b_backup)
        {
            b_backup = true;
            /* Setup the backup value if inner for loop fails. */
            // *p_log_addr_least_worn_page = cnt_pp * (NUM_LUTS_IN_PP + 1u) + 1u;
        }

        for (uint16_t lut_cnt = 0; lut_cnt < NUM_LUTS_IN_PP; ++lut_cnt)
        {
            /* If last LUT has been reached. */
            if (0 == memcmp(&last_dp_marker,
                            &temporary_pp.basic_data[lut_cnt].lut,
                            LUT_SIZE_BYTES))
            {
                break;
            }

            uint32_t worn_value = 0;
            memcpy(&worn_value, &temporary_pp.basic_data[lut_cnt].ect,
                   ECT_SIZE_BYTES);

            if (worn_value > min_worn_value)
            {
                min_worn_value = worn_value;
                p_wear_lvl->value_most_erased_dp = worn_value;
                /* This is currently the most worn out location. */
                *p_log_addr_most_worn_page = cnt_pp * (NUM_LUTS_IN_PP + 1u) +
                                             lut_cnt + 1u;
            }
        }
    }

    return fs_err;
}

static fs_err_code_t tl_write_page_wear_lvl(
        fs_data_page_t *p_data_page,
        uint32_t phy_dp_addr, uint32_t log_dp_addr,
        uint32_t phy_pp_addr, uint32_t log_pp_addr,
        uint32_t num_of_pp, uint16_t dp_lut_pos, page_type_t page_type,
        wear_lvl_t *p_wear_lvl, pages_info_t *p_page_info)
{
    fs_err_code_t fs_err;
    fs_partition_page_t partition_page;
    /* Read the PP containing DP's data. */
    uint32_t log_addr_pp = (log_dp_addr / (NUM_LUTS_IN_PP + 1u)) *
                           (NUM_LUTS_IN_PP + 1u);
    fs_err = tl_read_pp_flash(&partition_page, log_addr_pp,
                              p_wear_lvl, p_page_info);

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }
#if TL_DEBUG
    uint32_t ect = 0, next_pp = 0;
    memcpy(&ect, &partition_page.extended_data.erase_cnt, ECT_SIZE_BYTES);
    memcpy(&next_pp, &partition_page.extended_data.next_pp_addr, LUT_SIZE_BYTES);
    dprintf("\n==========================================\n");
    dprintf(" %d. PP info:\n"
            " ECT: %d,\tNext PP addr: %d\n",
            log_dp_addr / (NUM_LUTS_IN_PP + 1u), ect, next_pp);
#endif
    /* Delete this DP's location, */
    bool b_ret = fs_low_lvl_flash_erase(p_fs_flash, phy_dp_addr, 1);

    if (false == b_ret)
    {
        return FS_FLASH_ERASE_ERR;
    }
    /* Update the PP's data concerning DP which is being erased and store it
     * to a temporary copy. */
    tl_update_ect_dp(dp_lut_pos, &partition_page, p_wear_lvl);

    pp_page_info_t deleted_dp_info;
    memset(&deleted_dp_info, 0, sizeof(deleted_dp_info));
    deleted_dp_info.page_type = FREE;
    memcpy(&deleted_dp_info.ect, &partition_page.basic_data[dp_lut_pos].ect,
           ECT_SIZE_BYTES);
    memcpy(&deleted_dp_info.lut, &partition_page.basic_data[dp_lut_pos].lut,
           LUT_SIZE_BYTES);

    /** Find the free page that is less worn out that previous DP. */
    uint32_t free_dp_log_addr    = 0;
    uint32_t free_page_erase_cnt = 0;
    uint32_t last_dp_erase_cnt   = 0, cnt = 0;
    memcpy(&last_dp_erase_cnt, deleted_dp_info.ect.value, ECT_SIZE_BYTES);

    if ((last_dp_erase_cnt + 5) < p_wear_lvl->value_most_erased_dp)
    {
        /* Just reuse this page. */
        free_dp_log_addr = log_dp_addr;
    }
    else
    {
        do {
            /* Do a continuous search of a Free pages. */
            fs_err = tl_get_free_page_log_addr(&free_dp_log_addr, p_wear_lvl,
                                               p_page_info, true, 0);

            if (FS_NO_MEMORY_LEFT == fs_err)
            {
                /* Page that was just deleted is the only page left. */
                free_dp_log_addr = num_of_pp * (NUM_LUTS_IN_PP + 1u) +
                                   dp_lut_pos + 1u;
                /* Reset the error since it is a result of a continuous search. */
                fs_err = FS_ERR_OK;
                break;
            }
            else if (FS_ERR_OK != fs_err)
            {
                break;
            }
            /* Get erase count of a free page. */
            fs_err = tl_get_erase_cnt_dp(&free_page_erase_cnt, free_dp_log_addr,
                                         p_page_info);

            if (FS_ERR_OK != fs_err)
            {
                break;
            }

            if (cnt == p_page_info->num_free_dp)
            {
                /* The last page has been reached and the starting page is the
                 * least worn out page. */
                free_dp_log_addr = num_of_pp * (NUM_LUTS_IN_PP + 1u) +
                                   dp_lut_pos + 1u;
                break;
            }

            cnt++;
        } while (last_dp_erase_cnt < free_page_erase_cnt);
    }

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }

    /* Free page that is less worn out is now reached. Write data to it. */
    uint32_t free_dp_phy_addr = 0;
    fs_err = tl_get_phy_addr_dp(free_dp_log_addr, &free_dp_phy_addr, p_page_info);

    if (FS_ERR_OK == fs_err)
    {
        b_ret = fs_low_lvl_write_page(p_fs_flash, free_dp_phy_addr,
                                      p_data_page, 0);

        if (true != b_ret)
        {
            fs_err = FS_FLASH_WRITE_ERR;
        }
    }
    /** Update the PP pointing to the new and old DP. */
    uint32_t free_lut_pos = (free_dp_log_addr % (NUM_LUTS_IN_PP + 1u)) - 1u;

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }
    /* Get address of PP pointing to this DP. */
    uint32_t new_pp_phy_addr = 0;
    fs_err = tl_get_phy_addr_pp(free_dp_log_addr, &new_pp_phy_addr,
                                p_page_info, true);

    if (FS_ERR_OK != fs_err)
    {
        return fs_err;
    }

    if (new_pp_phy_addr == phy_pp_addr)
    {
        dprintf(" Old and new DP contained in same PP\n");
        /* Old and new DP are contained in the same PP. */
        /* Logical address of a free DP will now point via LUT to a
         * physical location of a deleted page and vice versa. */
        memcpy(&partition_page.basic_data[free_lut_pos],
               &deleted_dp_info, sizeof(pp_page_info_t));
#if TL_DEBUG
        uint32_t lut = 0, ect_dp = 0, pf = 0;
        memcpy(&lut, &partition_page.basic_data[free_lut_pos].lut,
               LUT_SIZE_BYTES);
        memcpy(&ect_dp, &partition_page.basic_data[free_lut_pos].ect,
               LUT_SIZE_BYTES);
        memcpy(&pf, &partition_page.basic_data[free_lut_pos].page_free, 1);
        dprintf(" 1st DP data, Data's position: %d\n"
                " LUT: %5d\t ECT: %5d\t PF: %d\n",free_lut_pos,lut, ect_dp, pf);
#endif
        /* Setup the data of DP with old logical address. */
        partition_page.basic_data[dp_lut_pos].page_type = page_type;
        /* Only update if this is not the same page! */
        if (dp_lut_pos != free_lut_pos)
        {
            memcpy(&partition_page.basic_data[dp_lut_pos].lut,
                   &free_dp_phy_addr, ECT_SIZE_BYTES);
            memcpy(&partition_page.basic_data[dp_lut_pos].ect,
                   &free_page_erase_cnt, ECT_SIZE_BYTES);
            tl_update_ect_dp(dp_lut_pos, &partition_page, p_wear_lvl);
        }

        tl_update_ect_pp(&partition_page); /* Updates the ECT of this PP. */
        snorfs_attach_md5_pp(&partition_page);
#if TL_DEBUG
        memcpy(&lut, &partition_page.basic_data[dp_lut_pos].lut,
               LUT_SIZE_BYTES);
        memcpy(&ect_dp, &partition_page.basic_data[dp_lut_pos].ect,
               LUT_SIZE_BYTES);
        memcpy(&pf, &partition_page.basic_data[dp_lut_pos].page_free, 1);
        dprintf(" 2nd DP data, Data's position: %d\n"
                " LUT: %5d\t ECT: %5d\t PF: %d\n",dp_lut_pos, lut, ect_dp, pf);
#endif
        /* Delete the PP. */
        b_ret = fs_low_lvl_flash_erase(p_fs_flash, phy_pp_addr, 1);

        if (true == b_ret)
        {
            /* Write the PP. */
            b_ret = fs_low_lvl_write_page(p_fs_flash,
                                          phy_pp_addr, &partition_page, 0);
        }
        else
        {
            fs_err = FS_FLASH_ERASE_ERR;
            b_ret = true; /* Don't enter next if case. */
        }

        if (true != b_ret)
        {
            fs_err = FS_FLASH_WRITE_ERR;
        }
    }
    else
    {
        /* Old and new DP are contained in different PPs. */
        dprintf(" Old and new DP contained in different PP\n");
        /* First handle the current PP (the one in which original DP is
         * pointed to). */
        partition_page.basic_data[dp_lut_pos].page_type = page_type;
        memcpy(&partition_page.basic_data[dp_lut_pos].ect,
               &free_page_erase_cnt, ECT_SIZE_BYTES);
        tl_update_ect_dp(dp_lut_pos, &partition_page, p_wear_lvl);
        memcpy(&partition_page.basic_data[dp_lut_pos].lut,
               &free_dp_phy_addr, ECT_SIZE_BYTES);

        tl_update_ect_pp(&partition_page); /* Updates the ECT of this PP. */
        snorfs_attach_md5_pp(&partition_page);
#if TL_DEBUG
        uint32_t lut = 0, ect_dp = 0, pf = 0;
        memcpy(&lut, &partition_page.basic_data[dp_lut_pos].lut,
               LUT_SIZE_BYTES);
        memcpy(&ect_dp, &partition_page.basic_data[dp_lut_pos].ect,
               LUT_SIZE_BYTES);
        memcpy(&pf, &partition_page.basic_data[dp_lut_pos].page_free, 1);


        dprintf(" 1st PP DP, Data's position: %d\n"
                " LUT: %5d\t ECT: %5d\t PF: %d\n", dp_lut_pos, lut, ect_dp, pf);
#endif
        /* Delete the current PP. */
        b_ret = fs_low_lvl_flash_erase(p_fs_flash, phy_pp_addr, 1);

        if (true == b_ret)
        {
            /* Write the first PP. */
            b_ret = fs_low_lvl_write_page(p_fs_flash,
                                          phy_pp_addr, &partition_page, 0);
        }
        else
        {
            return FS_FLASH_ERASE_ERR;
        }

        if (true != b_ret)
        {
            return FS_FLASH_WRITE_ERR;
        }
        /* Prepare data for next PP. */
        b_ret = fs_low_lvl_read_page(p_fs_flash,
                                     new_pp_phy_addr, &partition_page);

        if (false == b_ret)
        {
            return FS_FLASH_READ_ERR;
        }
        /* Copy the data from the start of function. */
        memcpy(&partition_page.basic_data[free_lut_pos], &deleted_dp_info,
               sizeof(deleted_dp_info));
#if TL_DEBUG
        memcpy(&lut, &partition_page.basic_data[free_lut_pos].lut,
               LUT_SIZE_BYTES);
        memcpy(&ect_dp, &partition_page.basic_data[free_lut_pos].ect,
               LUT_SIZE_BYTES);
        memcpy(&pf, &partition_page.basic_data[free_lut_pos].page_free, 1);

        dprintf(" 2nd PP DP, Data's position: %d\n"
                " LUT: %5d\t ECT: %5d\t PF: %d\n",free_lut_pos,lut, ect_dp, pf);
#endif
        /* Update the ECT of this PP. */
        tl_update_ect_pp(&partition_page);
        snorfs_attach_md5_pp(&partition_page);
        /* Burn this PP; First delete it. */
        b_ret = fs_low_lvl_flash_erase(p_fs_flash, new_pp_phy_addr, 1);

        if (true == b_ret)
        {
            /* Write the first PP. */
            b_ret = fs_low_lvl_write_page(p_fs_flash,
                                          new_pp_phy_addr, &partition_page, 0);
        }
        else
        {
            fs_err = FS_FLASH_ERASE_ERR;
        }

        if (true != b_ret)
        {
            fs_err = FS_FLASH_WRITE_ERR;
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Now that the DP has been burned and one or two PPs have been
         * updated with new ECT and LUT (and page free or not), see if
         * updated PP(s) erase count exceeds allowed value. If so, exchange
         * that PP with least worn out page. Again, two cases possible. */
        if (new_pp_phy_addr == phy_pp_addr)
        {
            /* Only one PP to Check, and if needed wear level. */
            fs_err = tl_check_and_wear_lvl_pp(&partition_page,
                                              phy_pp_addr, log_pp_addr,
                                              p_wear_lvl, p_page_info);
        }
        else
        {
            uint32_t new_pp_log_addr = free_dp_log_addr / (NUM_LUTS_IN_PP + 1u);
            new_pp_log_addr         *= (NUM_LUTS_IN_PP + 1u);
            /* Two PPs for wear leveling. */
            fs_err = tl_check_and_wear_lvl_pp(&partition_page,
                                              new_pp_phy_addr, new_pp_log_addr,
                                              p_wear_lvl, p_page_info);

            if (FS_ERR_OK == fs_err)
            {
                /* Read the first PP. */
                b_ret = fs_low_lvl_read_page(p_fs_flash, phy_pp_addr,
                                             &partition_page);
                if (true == b_ret)
                {
                    fs_err = tl_check_and_wear_lvl_pp(&partition_page,
                                                      phy_pp_addr, log_pp_addr,
                                                      p_wear_lvl, p_page_info);
                }
            }
        }
    }

    uint32_t erase_cnt = 0;

    if (FS_ERR_OK == fs_err)
    {
        fs_err = tl_get_erase_cnt_dp(&erase_cnt, log_dp_addr, p_page_info);
    }

    if (FS_ERR_OK == fs_err)
    {
        if (erase_cnt >= (p_wear_lvl->value_least_erased_dp +
                          STATIC_WEAR_LVL_UPPER_ERASE_LIMIT))
        {
            uint32_t pp_containing_dp_addr;
            tl_get_phy_addr_pp(log_dp_addr, &pp_containing_dp_addr,
                               p_page_info, true);
            b_ret = fs_low_lvl_read_page(p_fs_flash, phy_pp_addr,
                                         &partition_page);

            if (true == b_ret)
            {
                /* Exchange two DPs. */
                fs_err = tl_exchange_dp(log_dp_addr, &partition_page,
                                        p_wear_lvl, p_page_info);
            }
            else
            {
                fs_err = FS_FLASH_READ_ERR;
            }
        }
    }

    return fs_err;
}

static void tl_update_lut_dp(uint16_t dp_lut_pos, uint32_t *p_new_dp_phy_addr,
                             fs_partition_page_t *p_temp_pp)
{
    memcpy(&p_temp_pp->basic_data[dp_lut_pos].ect, p_new_dp_phy_addr,
           LUT_SIZE_BYTES);
}

static fs_err_code_t tl_get_phy_addr_pp (uint32_t log_addr_dp,
                                         uint32_t *p_phy_pp_addr,
                                         pages_info_t *p_page_info,
                                         bool b_fast_search)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    /* This value contains number of PP which contains LUT for searched DP. */
    uint32_t num_of_pp = log_addr_dp / (NUM_LUTS_IN_PP + 1u);

    if (num_of_pp >= p_page_info->num_pp)
    {
        return FS_UNSUPPORTED_PAGE_ADDR;
    }

    if (true == b_fast_search)
    {
        memcpy(p_phy_pp_addr, &p_page_info->addr_pp[num_of_pp], LUT_SIZE_BYTES);
        return FS_ERR_OK;
    }

    uint32_t curr_pp_addr = p_page_info->starting_pp_phy_addr;
    fs_partition_page_t temp_pp;
    /* Loop can be avoided if the searched PP is the first (starting) one. */
    for (uint32_t cnt_pp = 0; cnt_pp < num_of_pp; ++cnt_pp)
    {
        memset(&temp_pp, 0xff, sizeof(temp_pp));
        bool b_ret = fs_low_lvl_read_page (p_fs_flash, curr_pp_addr,
                                           &temp_pp);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_READ_ERR;
            break;
        }

        memcpy(&curr_pp_addr, &temp_pp.extended_data.next_pp_addr,
               LUT_SIZE_BYTES);
    }
    /* Copy the address of the current PP. */
    *p_phy_pp_addr = curr_pp_addr;
    return fs_err;
}

static fs_err_code_t tl_get_phy_addr_dp (uint32_t log_addr_dp,
                                       uint32_t *p_phy_dp_addr,
                                       pages_info_t *p_page_info) {
    fs_err_code_t fs_err = FS_ERR_OK;
    /* This value contains number of PP which contains LUT for searched DP. */
    uint32_t num_of_pp = log_addr_dp / (NUM_LUTS_IN_PP + 1u);

    if (num_of_pp > p_page_info->num_pp) {
        return FS_UNSUPPORTED_PAGE_ADDR;
    }

    /* This value contains location of DP's data in PP pointing to that DP. */
    uint32_t dp_lut_pos = (log_addr_dp % (NUM_LUTS_IN_PP + 1u)) - 1u;

    uint32_t curr_pp_addr = 0;
    fs_err = tl_get_phy_addr_pp(log_addr_dp, &curr_pp_addr, p_page_info, true);

    static fs_partition_page_t temp_part_page;

    bool b_ret = fs_low_lvl_read_page(p_fs_flash, curr_pp_addr,
                                      &temp_part_page);

    if (false == b_ret)
    {
        fs_err = FS_FLASH_READ_ERR;
    }
    /* When the PP pointing to desired DP has been reached. */
    if (FS_ERR_OK == fs_err)
    {
        /* Copy the physical address of the DP. */
        memcpy(p_phy_dp_addr, &temp_part_page.basic_data[dp_lut_pos].lut,
               LUT_SIZE_BYTES);


        /* Prepare physical address of the next PP/ */
        memcpy(&curr_pp_addr, &temp_part_page.extended_data.next_pp_addr,
               LUT_SIZE_BYTES);
    }

    return fs_err;
}

static void tl_update_ect_pp(fs_partition_page_t *p_partition_page)
{
    uint32_t pp_erase_cnt;
    memcpy(&pp_erase_cnt, &p_partition_page->extended_data.erase_cnt,
           ECT_SIZE_BYTES);
    /* Update ECT of Partition page. */
    pp_erase_cnt++;
    memcpy(&p_partition_page->extended_data.erase_cnt, &pp_erase_cnt,
           ECT_SIZE_BYTES);
}

static fs_err_code_t tl_get_erase_cnt_dp(uint32_t *p_erase_cnt,
                                         uint32_t dp_log_addr,
                                         pages_info_t *p_page_info)
{
    fs_err_code_t fs_err;

    uint32_t pp_phy_addr;
    fs_err = tl_get_phy_addr_pp(dp_log_addr, &pp_phy_addr, p_page_info, true);

    if (FS_ERR_OK == fs_err)
    {
        uint16_t dp_lut_pos = (uint16_t)
                ((dp_log_addr % (NUM_LUTS_IN_PP + 1u)) - 1u);

        pp_page_info_t page_info;

        bool b_ret = fs_low_lvl_read_dp_info(p_fs_flash, pp_phy_addr,
                                             dp_lut_pos, &page_info);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_READ_ERR;
        }
        else
        {
            memcpy(p_erase_cnt, &page_info.ect, ECT_SIZE_BYTES);
        }
    }

    return fs_err;
}

static fs_err_code_t tl_check_and_wear_lvl_pp(
        fs_partition_page_t *p_partition_page,
        uint32_t pp_phy_addr, uint32_t pp_log_addr,
        wear_lvl_t *p_wear_lvl, pages_info_t *p_page_info)
{
    fs_err_code_t fs_err = FS_ERR_OK;
    uint32_t    erase_count = 0;

    bool b_ret = fs_low_lvl_read_page(p_fs_flash,
                                      pp_phy_addr, p_partition_page);

    if (false == b_ret)
    {
        return FS_FLASH_READ_ERR;
    }
#if TL_DEBUG
    uint32_t ect = 0, next_pp = 0;
    memcpy(&ect, &p_partition_page->extended_data.erase_cnt, ECT_SIZE_BYTES);
    memcpy(&next_pp, &p_partition_page->extended_data.next_pp_addr,
           LUT_SIZE_BYTES);
    dprintf(" PP info:\n"
            " Name: %2s,\t ECT: %d,\tNext PP addr: %d\n",
            p_partition_page->extended_data.name_pp, ect, next_pp);
#endif
    /* Find out erase count value of the PP. */
    memcpy(&erase_count, &p_partition_page->extended_data.erase_cnt,
           ECT_SIZE_BYTES);

    if (erase_count < (p_wear_lvl->value_least_erased_dp +
                       ALLOWED_ERASE_CNT_PP_OVERVALUE))
    {
        /* Current PP doesn't require wear leveling. */
        return fs_err;
    }

    dprintf(" Wear leveling PP!\n");
    /* Re-acquire least deleted page's erase count value. */
    uint32_t least_worn_page_log_addr = 0;
    fs_err = tl_find_least_worn_dp(&least_worn_page_log_addr,
                                   p_wear_lvl, p_page_info, false);
    /** If erase count of PP exceeds allowed value, wear level it. */

    fs_data_page_t data_page;
    uint32_t       least_worn_page_phy_addr = 0;

    if (FS_ERR_OK == fs_err)
    {
        /* Find physical address of least worn DP and read it. */
        fs_err = tl_get_phy_addr_dp(least_worn_page_log_addr,
                                    &least_worn_page_phy_addr, p_page_info);

        b_ret = fs_low_lvl_read_page(p_fs_flash,
                                     least_worn_page_phy_addr, &data_page);
        /* Attach MD5 in case of a free DP - writing check. */
        snorfs_attach_md5_dp(&data_page);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_READ_ERR;
        }

        dprintf("\n---Exchanging PP (Phy addr: %d) and DP (Phy addr: %d)---\n",
                pp_phy_addr, least_worn_page_phy_addr);
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Update the PPs address in the pages_info structure. */
        uint32_t pp_pos = pp_log_addr / (NUM_LUTS_IN_PP + 1u);
        memcpy(&p_page_info->addr_pp[pp_pos], &least_worn_page_phy_addr,
               LUT_SIZE_BYTES);
    }

    uint32_t       pp_containing_dp_phy_addr = 0;
    uint16_t       lut_pos_least_worn        = 0;
    pp_page_info_t page_info_least_worn;

    if (FS_ERR_OK == fs_err)
    {
        /* Find Physical address of PP containing pointer to least worn DP. */
        fs_err = tl_get_phy_addr_pp(least_worn_page_log_addr,
                                    &pp_containing_dp_phy_addr, p_page_info,
                                    true);
    }
    /* Exchange positions of PP and least worn DP: burn PP to DP and
     * vice versa. */
    if (FS_ERR_OK == fs_err)
    {
        b_ret = fs_low_lvl_flash_erase(p_fs_flash, pp_phy_addr, 1);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_ERASE_ERR;
        }
        else
        {
            /* Write DP to location of worn out PP. */
            b_ret = fs_low_lvl_write_page(p_fs_flash, pp_phy_addr,
                                          &data_page, 0);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }
        /* Prepare LUT, ECT data for PP containing DP's address. */
        lut_pos_least_worn = (uint16_t) ((least_worn_page_log_addr %
                                         (NUM_LUTS_IN_PP + 1u)) - 1u);
        /* Store the value */
        memcpy(&page_info_least_worn.lut, &pp_phy_addr, LUT_SIZE_BYTES);
        /* Erase count of PP is now erase count of DP. */
        memcpy(&page_info_least_worn.ect, &erase_count, ECT_SIZE_BYTES);

        uint32_t lut_pos_dp = least_worn_page_log_addr % (NUM_LUTS_IN_PP + 1u);
        memcpy(&page_info_least_worn.page_type,
               &p_partition_page->basic_data[lut_pos_dp].page_type,
               sizeof(page_type_t));
    }

    if (FS_ERR_OK == fs_err)
    {
        if (pp_containing_dp_phy_addr == pp_phy_addr)
        {
            /* Least worn page is contained in the PP that is sent to
             * exchange. Data about DP is copied in the PP's basic data. */
            memcpy(&p_partition_page->basic_data[lut_pos_least_worn],
                   &page_info_least_worn, sizeof(page_info_least_worn));
            tl_update_ect_dp(lut_pos_least_worn, p_partition_page, p_wear_lvl);
#if TL_DEBUG
            uint32_t lut = 0, pf = 0;
            ect = 0;
            memcpy(&lut, &p_partition_page->basic_data[lut_pos_least_worn].lut,
                   LUT_SIZE_BYTES);
            memcpy(&ect, &p_partition_page->basic_data[lut_pos_least_worn].ect,
                   LUT_SIZE_BYTES);
            memcpy(&pf, &p_partition_page->basic_data[lut_pos_least_worn].page_free, 1);
            dprintf(" -------- PP contains DP --------\n");
            dprintf(" pages_info copied to least worn position:\n"
                    " LUT: %d\t ECT:%d\t PF: %d\n", lut, ect, pf);
#endif
        }
        /* ECT is updated for any case. */
        memcpy(&p_partition_page->extended_data.erase_cnt,
               &p_wear_lvl->value_least_erased_dp, ECT_SIZE_BYTES);
        tl_update_ect_pp(p_partition_page);
        snorfs_attach_md5_pp(p_partition_page);
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Burn the PP to DP's location. */
        b_ret = fs_low_lvl_flash_erase(p_fs_flash, least_worn_page_phy_addr, 1);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_ERASE_ERR;
        }
        else
        {
            b_ret = fs_low_lvl_write_page(p_fs_flash, least_worn_page_phy_addr,
                                          p_partition_page, 0);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }
    }
    /* PP and least worn page have physically exchanged positions, update LUT's
     * and ECT's to follow. */
    if (FS_ERR_OK == fs_err)
    {
        if (pp_containing_dp_phy_addr != pp_phy_addr)
        {
            /* Use old PP variable to read to, since it's data won't be used
             * anymore - memory saving. */
            b_ret = fs_low_lvl_read_page(p_fs_flash, pp_containing_dp_phy_addr,
                                         p_partition_page);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_READ_ERR;
            }
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        if (pp_containing_dp_phy_addr != pp_phy_addr)
        {
            /* Prepare updated data to write. */
            memcpy(&p_partition_page->basic_data[lut_pos_least_worn].lut,
                   &page_info_least_worn.lut, LUT_SIZE_BYTES);
            memcpy(&p_partition_page->basic_data[lut_pos_least_worn].ect,
                   &page_info_least_worn.ect, ECT_SIZE_BYTES);
            tl_update_ect_dp(lut_pos_least_worn, p_partition_page, p_wear_lvl);
            /* Update ECT of PP. */
            tl_update_ect_pp(p_partition_page);
            snorfs_attach_md5_pp(p_partition_page);
#if TL_DEBUG
            uint32_t lut = 0, pf = 0;
            ect = 0;
            memcpy(&lut, &p_partition_page->basic_data[lut_pos_least_worn].lut,
                   LUT_SIZE_BYTES);
            memcpy(&ect, &p_partition_page->basic_data[lut_pos_least_worn].ect,
                   LUT_SIZE_BYTES);
            memcpy(&pf, &p_partition_page->basic_data[lut_pos_least_worn].page_free, 1);
            dprintf(" pages_info copied to PP least worn position:\n"
                    " LUT: %d\t ECT: %d\t PF: %d\n", lut, ect, pf);
#endif
            /* Refresh the PP, burn it to same location. If this erase process
             * makes this PP too much worn out, eventually some DP contained
             * in it will be updated and PP will be moved to less worn out
             * location. */
            b_ret = fs_low_lvl_flash_erase(p_fs_flash,
                                           pp_containing_dp_phy_addr, 1);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_ERASE_ERR;
            }
            else
            {
                b_ret = fs_low_lvl_write_page(p_fs_flash,
                                              pp_containing_dp_phy_addr,
                                              p_partition_page, 0);

                if (false == b_ret)
                {
                    fs_err = FS_FLASH_WRITE_ERR;
                }
            }
        }
    }
    /** Update PP pointing to exchanged PP - unnecessary if the PP is the
     *  first one.*/
    if (pp_phy_addr != p_page_info->starting_pp_phy_addr)
    {
        uint32_t pp_containing_pp_phy_addr = 0;

        if (FS_ERR_OK == fs_err)
        {
            /* It only remains to update PP pointing to just moved PP (the one
             * that was wear leveled). */
            uint32_t temp_addr = pp_phy_addr;
            fs_err = tl_find_pp_containing_pp(&temp_addr,
                                              &pp_containing_pp_phy_addr,
                                              p_page_info);
        }

        if (FS_ERR_OK == fs_err)
        {
            /* Read this PP. */
            b_ret = fs_low_lvl_read_page(p_fs_flash, pp_containing_pp_phy_addr,
                                         p_partition_page);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_READ_ERR;
            }
        }

        if (FS_ERR_OK == fs_err)
        {
            /* Update data and burn it. */
            memcpy(&p_partition_page->extended_data.next_pp_addr,
                   &least_worn_page_phy_addr, LUT_SIZE_BYTES);
            tl_update_ect_pp(p_partition_page);
            snorfs_attach_md5_pp(p_partition_page);
#if TL_DEBUG
            uint32_t er_cnt = 0; next_pp = 0;
            memcpy(&er_cnt, &p_partition_page->extended_data.erase_cnt, 3);
            memcpy(&next_pp, &p_partition_page->extended_data.next_pp_addr, 3);
            dprintf(" Updating PP pointing to PP:\n"
                    " Next PP addr: %d\t, ECT: %d\n", next_pp, er_cnt);
#endif
            b_ret = fs_low_lvl_flash_erase(p_fs_flash,
                                           pp_containing_pp_phy_addr, 1);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_ERASE_ERR;
            }
            else
            {
                b_ret = fs_low_lvl_write_page(p_fs_flash,
                                              pp_containing_pp_phy_addr,
                                              p_partition_page, 0);

                if (false == b_ret)
                {
                    fs_err = FS_FLASH_WRITE_ERR;
                }
            }
        }
    }
    else
    {
        /* If the exchanged PP is the starting one, update PP's info. */
        p_page_info->starting_pp_phy_addr = least_worn_page_phy_addr;
    }

    if (FS_ERR_OK != fs_err)
    {
        fs_err = FS_FLASH_CORRUPTED;
    }

    return fs_err;
}

fs_err_code_t tl_find_pp_containing_pp(uint32_t *p_pp_phy_addr,
                                     uint32_t *p_pp_containing_pp_phy_addr,
                                     pages_info_t *p_page_info)
{
    fs_err_code_t fs_err       = FS_ERR_OK;
    uint32_t    curr_pp_addr = p_page_info->starting_pp_phy_addr;
    bool        b_ret;

    static fs_partition_page_t temp_pp;
    /* Search the PP's for address. */
    for (uint32_t cnt_pp = 0; cnt_pp <= p_page_info->num_pp; ++cnt_pp)
    {
        memset(&temp_pp, 0xff, sizeof(temp_pp));

        b_ret = fs_low_lvl_read_page(p_fs_flash, curr_pp_addr, &temp_pp);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_READ_ERR;
            break;
        }

        if (0 == memcmp(&temp_pp.extended_data.next_pp_addr, p_pp_phy_addr,
                        LUT_SIZE_BYTES))
        {
            /* Current PP is the one pointing to the desired one. */
            *p_pp_containing_pp_phy_addr = curr_pp_addr;
            break;
        }

        memcpy(&curr_pp_addr, &temp_pp.extended_data.next_pp_addr,
               LUT_SIZE_BYTES);
    }

    return fs_err;
}

static fs_err_code_t tl_exchange_dp(uint32_t log_addr_most_worn_dp,
                                    fs_partition_page_t *p_partition_page,
                                    wear_lvl_t *p_wear_lvl,
                                    pages_info_t *p_page_info)
{
    fs_err_code_t fs_err;
    /* Find the least worn out DP. */
    uint32_t log_addr_least_worn_dp = 0;
    fs_err = tl_find_least_worn_dp_inversed(&log_addr_least_worn_dp,
                                            p_wear_lvl, p_page_info);

    fs_data_page_t data_page;
    uint32_t       phy_addr_most_worn_pp = 0, phy_addr_least_worn_pp = 0;
    uint32_t       phy_addr_most_worn_dp = 0, phy_addr_least_worn_dp = 0;
    bool           b_ret;

    if (FS_ERR_OK == fs_err)
    {
        /* Find the PP pointing to the most worn out DP. */
        fs_err = tl_get_phy_addr_pp(log_addr_most_worn_dp,
                                    &phy_addr_most_worn_pp, p_page_info, true);
        /* Find the DP's physical address. */
        if (FS_ERR_OK == fs_err)
        {
            fs_err = tl_get_phy_addr_dp(log_addr_most_worn_dp,
                                        &phy_addr_most_worn_dp, p_page_info);
        }

        if (FS_ERR_OK == fs_err)
        {
            /* Now read it. */
            b_ret = fs_low_lvl_read_page(p_fs_flash,
                                         phy_addr_most_worn_dp, &data_page);
            /* Attach MD5 in case the DP is free. */
            snorfs_attach_md5_dp(&data_page);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_READ_ERR;
            }
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Find the PP pointing to the least worn out DP. */
        fs_err = tl_get_phy_addr_pp(log_addr_least_worn_dp,
                                    &phy_addr_least_worn_pp, p_page_info, true);
        /* Find the DP's physical address. */
        if (FS_ERR_OK == fs_err)
        {
            fs_err = tl_get_phy_addr_dp(log_addr_least_worn_dp,
                                        &phy_addr_least_worn_dp, p_page_info);
        }

        if (FS_ERR_OK == fs_err)
        {
            /* Now read it. */
            b_ret = fs_low_lvl_read_page(p_fs_flash,
                                         phy_addr_least_worn_dp,
                                         (fs_data_page_t *) p_partition_page);
            /* Dirty hack on cast - possible because both structures contain
             * equal number of bytes. p_partition_page now contains least
             * worn out DP's data. */
            snorfs_attach_md5_dp((fs_data_page_t *) p_partition_page);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_READ_ERR;
            }
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Write most worn out DP out to the location of least worn out DP. */
        b_ret = fs_low_lvl_flash_erase(p_fs_flash, phy_addr_least_worn_dp, 1);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_ERASE_ERR;
        }
        else
        {
            b_ret = fs_low_lvl_write_page(p_fs_flash, phy_addr_least_worn_dp,
                                          &data_page, 0);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        /* Write least worn out DP out to the location of most worn out DP. */
        b_ret = fs_low_lvl_flash_erase(p_fs_flash, phy_addr_most_worn_dp, 1);

        if (false == b_ret)
        {
            fs_err = FS_FLASH_ERASE_ERR;
        }
        else
        {
            b_ret = fs_low_lvl_write_page(p_fs_flash, phy_addr_most_worn_dp,
                                          p_partition_page, 0);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }
    }

    if (FS_ERR_OK != fs_err)
    {
        /* At this point two DPs are already lost, don't mess up the
         * partition pages! */
        return fs_err;
    }

    dprintf("\nExchanged these two DPs: Phy addr %d with phy addr %d.\n",
            phy_addr_least_worn_dp, phy_addr_most_worn_dp);
    uint32_t lut_pos_most_worn, lut_pos_least_worn, log_pp_addr;
    /* Update PPs of those two DPs. */
    if (phy_addr_least_worn_pp == phy_addr_most_worn_pp)
    {
        dprintf("Same PP\n");
        /* Both DPs are contained inside one PP. */
        lut_pos_least_worn = (log_addr_least_worn_dp % (NUM_LUTS_IN_PP + 1u))
                              - 1u;
        lut_pos_most_worn  = (log_addr_most_worn_dp % (NUM_LUTS_IN_PP + 1u)) -
                              1u;
        /* Read the PP. */
        log_pp_addr = log_addr_least_worn_dp / (NUM_LUTS_IN_PP + 1u);
        log_pp_addr *= NUM_LUTS_IN_PP + 1u;
        fs_err = tl_read_pp_flash(p_partition_page, log_pp_addr,
                                  p_wear_lvl, p_page_info);

        if (FS_ERR_OK == fs_err)
        {
            /* Update data! */
            pp_page_info_t temp_info;
            memcpy(&temp_info, &p_partition_page->basic_data[lut_pos_most_worn],
                   sizeof(temp_info));

            memcpy(&p_partition_page->basic_data[lut_pos_most_worn],
                   &p_partition_page->basic_data[lut_pos_least_worn],
                   sizeof(temp_info));
            tl_update_ect_dp((uint16_t) lut_pos_most_worn, p_partition_page,
                             p_wear_lvl);

            memcpy(&p_partition_page->basic_data[lut_pos_least_worn],
                   &temp_info, sizeof(temp_info));
            tl_update_ect_dp((uint16_t) lut_pos_least_worn,
                             p_partition_page, p_wear_lvl);
            /* Update ECT of PP. */
            tl_update_ect_pp(p_partition_page);
            snorfs_attach_md5_pp(p_partition_page);
            /* Burn the PP! */
            b_ret = fs_low_lvl_flash_erase(p_fs_flash,
                                           phy_addr_most_worn_pp, 1);

            if (false == b_ret)
            {
                fs_err = FS_FLASH_ERASE_ERR;
            }
            else
            {
                b_ret = fs_low_lvl_write_page(p_fs_flash, phy_addr_most_worn_pp,
                                              p_partition_page, 0);

                if (false == b_ret)
                {
                    fs_err = FS_FLASH_WRITE_ERR;
                }
            }
        }
    }
    else
    {
        dprintf("Separated PPs\n");
        /* Separated PPs for both DPs. First update the DP's data. */
        lut_pos_least_worn = (log_addr_least_worn_dp % (NUM_LUTS_IN_PP + 1u))
                              - 1u;
        lut_pos_most_worn  = (log_addr_most_worn_dp % (NUM_LUTS_IN_PP + 1u))
                             - 1u;

        /* Read the PP containing most worn out DP. Acquire multiple of 74 to
         * get PP's logical address. */
        log_pp_addr = log_addr_most_worn_dp / (NUM_LUTS_IN_PP + 1u);
        log_pp_addr *= (NUM_LUTS_IN_PP + 1u);
        fs_err = tl_read_pp_flash(p_partition_page, log_pp_addr,
                                  p_wear_lvl, p_page_info);
        static fs_partition_page_t least_worn_pp;

        pp_page_info_t temp_info_most_worn, temp_info_least_worn;
        memset(&temp_info_least_worn, 0, sizeof(temp_info_least_worn));
        memset(&temp_info_most_worn, 0, sizeof(temp_info_most_worn));
        page_type_t least_worn_pt, most_worn_pt;

        if (FS_ERR_OK == fs_err)
        {
            /* Copy the most worn out DP's data. */
            tl_update_ect_dp((uint16_t) lut_pos_most_worn, p_partition_page,
                             p_wear_lvl);
            memcpy(&temp_info_most_worn,
                   &p_partition_page->basic_data[lut_pos_most_worn],
                   sizeof(temp_info_most_worn));
            most_worn_pt = temp_info_most_worn.page_type;

            /* Now read the PP containing Least worn out DP and update it.*/
            log_pp_addr = log_addr_least_worn_dp / (NUM_LUTS_IN_PP + 1u);
            log_pp_addr *= (NUM_LUTS_IN_PP + 1u);

            fs_err = tl_read_pp_flash(&least_worn_pp, log_pp_addr,
                                      p_wear_lvl, p_page_info);
        }

        if (FS_ERR_OK == fs_err)
        {
            /* Copy the least worn out DP's data. */
            tl_update_ect_dp((uint16_t) lut_pos_least_worn,
                             &least_worn_pp, p_wear_lvl);
            memcpy(&temp_info_least_worn,
                   &least_worn_pp.basic_data[lut_pos_least_worn],
                   sizeof(temp_info_least_worn));
            least_worn_pt = temp_info_least_worn.page_type;
            /* Exchange page type. */
            temp_info_least_worn.page_type = most_worn_pt;
            temp_info_most_worn.page_type  = least_worn_pt;
            /* Copy most worn out DP's data in least worn out DP's data. */
            memcpy(&least_worn_pp.basic_data[lut_pos_least_worn],
                   &temp_info_most_worn, sizeof(temp_info_most_worn));
            /* Update ECT of PP containing least worn out DP. */
            tl_update_ect_pp(&least_worn_pp);
            snorfs_attach_md5_pp(&least_worn_pp);
            /* Erase and write that PP. */
            b_ret = fs_low_lvl_flash_erase(p_fs_flash,
                                           phy_addr_least_worn_pp, 1);

            if (true == b_ret)
            {
                /* Write the PP! */
                b_ret = fs_low_lvl_write_page(p_fs_flash, phy_addr_least_worn_pp,
                                              &least_worn_pp, 0);
            }
            else
            {
                fs_err = FS_FLASH_ERASE_ERR;
            }

            if (false == b_ret)
            {
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }

        if (FS_ERR_OK == fs_err)
        {
            /* Copy least worn out DP's data in most worn out DP's data.*/
            memcpy(&p_partition_page->basic_data[lut_pos_most_worn],
                   &temp_info_least_worn, sizeof(temp_info_least_worn));
            /* Update the PP's ECT. */
            tl_update_ect_pp(p_partition_page);
            snorfs_attach_md5_pp(p_partition_page);
            /* Erase and write that PP. */
            b_ret = fs_low_lvl_flash_erase(p_fs_flash,
                                           phy_addr_most_worn_pp, 1);

            if (true == b_ret)
            {
                /* Write the PP! */
                b_ret = fs_low_lvl_write_page(p_fs_flash, phy_addr_most_worn_pp,
                                              p_partition_page, 0);
            }
            else
            {
                fs_err = FS_FLASH_ERASE_ERR;
            }

            if (false == b_ret)
            {
                fs_err = FS_FLASH_WRITE_ERR;
            }
        }
    }

    if (FS_ERR_OK == fs_err)
    {
        p_wear_lvl->log_addr_most_worn_dp = log_addr_most_worn_dp;
    }

    dprintf("\n");
    return fs_err;
}
//--------------------------- INTERRUPT HANDLERS ------------------------------
