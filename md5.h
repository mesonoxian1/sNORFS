/** @file md5.h
*
* @brief See source file.
*
* @par
* COPYRIGHT NOTICE: (c) 2018 Mihael Varga
* All rights reserved.
*/

#ifndef MY_AWESOME_DIPLOMSKI_MY_FS_MD5_H
#define MY_AWESOME_DIPLOMSKI_MY_FS_MD5_H

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------ INCLUDES -------------------------------------
#include <stdint.h>
#include <stddef.h>


//-------------------------- CONSTANTS & MACROS -------------------------------

//----------------------------- DATA TYPES ------------------------------------

//---------------------- PUBLIC FUNCTION PROTOTYPES ---------------------------
/**@brief Calculate the MD5 hash.
 *
 * @param initial_msg
 * @param initial_len
 * @param p_temp_data
 */
void calc_md5(uint8_t *initial_msg, size_t initial_len, uint8_t *p_temp_data);

#ifdef __cplusplus
}
#endif

#endif //MY_AWESOME_DIPLOMSKI_MY_FS_MD5_H
